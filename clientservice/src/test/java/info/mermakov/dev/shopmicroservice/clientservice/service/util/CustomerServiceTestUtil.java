package info.mermakov.dev.shopmicroservice.clientservice.service.util;

import info.mermakov.dev.shopmicroservice.clientservice.dto.CustomerDto;
import info.mermakov.dev.shopmicroservice.clientservice.model.Customer;

public class CustomerServiceTestUtil {
    public static final Customer CUSTOMER_1;
    public static final Customer CUSTOMER_2;
    public static final CustomerDto CUSTOMER_DTO_1;
    public static final CustomerDto CUSTOMER_DTO_2;

    static {
        CUSTOMER_1 = new Customer();
        CUSTOMER_2 = new Customer();
        CUSTOMER_DTO_1 = new CustomerDto();
        CUSTOMER_DTO_2 = new CustomerDto();
        initData();
    }

    public static void initData() {
        CUSTOMER_1.setId(1L);
        CUSTOMER_1.setFirstName("Ivan");
        CUSTOMER_1.setMiddleName("Ivanovich");
        CUSTOMER_1.setLastName("Ivanov");
        CUSTOMER_1.setEmail("test@test.test");

        CUSTOMER_2.setId(2L);
        CUSTOMER_2.setFirstName("Petr");
        CUSTOMER_2.setLastName("Petrov");
        CUSTOMER_2.setEmail("test2@test.test");

        CUSTOMER_DTO_1.setId(1L);
        CUSTOMER_DTO_1.setFirstName("Ivan");
        CUSTOMER_DTO_1.setMiddleName("Ivanovich");
        CUSTOMER_DTO_1.setLastName("Ivanov");
        CUSTOMER_DTO_1.setEmail("test@test.test");

        CUSTOMER_DTO_2.setId(2L);
        CUSTOMER_DTO_2.setFirstName("Petr");
        CUSTOMER_DTO_2.setLastName("Petrov");
        CUSTOMER_DTO_2.setEmail("test2@test.test");
    }
}
