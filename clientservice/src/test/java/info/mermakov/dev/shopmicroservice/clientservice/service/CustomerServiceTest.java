package info.mermakov.dev.shopmicroservice.clientservice.service;

import info.mermakov.dev.shopmicroservice.clientservice.dto.CustomerDto;
import info.mermakov.dev.shopmicroservice.clientservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.clientservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.clientservice.model.Customer;
import info.mermakov.dev.shopmicroservice.clientservice.model.Order;
import info.mermakov.dev.shopmicroservice.clientservice.service.util.CustomerServiceTestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static info.mermakov.dev.shopmicroservice.clientservice.service.util.CustomerServiceTestUtil.CUSTOMER_1;
import static info.mermakov.dev.shopmicroservice.clientservice.service.util.CustomerServiceTestUtil.CUSTOMER_2;
import static info.mermakov.dev.shopmicroservice.clientservice.service.util.CustomerServiceTestUtil.CUSTOMER_DTO_1;
import static info.mermakov.dev.shopmicroservice.clientservice.service.util.CustomerServiceTestUtil.CUSTOMER_DTO_2;
import static info.mermakov.dev.shopmicroservice.clientservice.service.util.OrderServiceTestUtil.ORDER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application.properties")
public class CustomerServiceTest extends ServiceTest {
    @Autowired
    private CustomerService service;

    private List<Customer> customers;

    @BeforeEach
    public void init() {
        CustomerServiceTestUtil.initData();

        customers = new ArrayList<>();
        customers.add(CUSTOMER_1);
        customers.add(CUSTOMER_2);
    }

    @Test
    public void findAllTest() {
        Mockito.when(customerRepository.findAll()).thenReturn(customers);
        List<CustomerDto> result = service.findAll();
        assertEquals(2, result.size());
        assertTrue(result.contains(CUSTOMER_DTO_1));
        assertTrue(result.contains(CUSTOMER_DTO_2));
    }

    @Test
    public void findAllExceptionTest() {
        Mockito.when(customerRepository.findAll()).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> service.findAll());
    }

    @Test
    public void findByIdTest() {
        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(CUSTOMER_1));
        assertEquals(CUSTOMER_DTO_1, service.findById(1L));
    }

    @Test
    public void findByIdExceptionTest() {
        Mockito.when(customerRepository.findById(2L)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> service.findById(2L));
    }

    @Test
    public void findByEmailTest() {
        Mockito.when(customerRepository.findByEmail("test@test.test")).thenReturn(Optional.of(CUSTOMER_1));
        assertEquals(CUSTOMER_DTO_1, service.findByEmail("test@test.test"));
    }

    @Test
    public void findByEmailExceptionTest() {
        assertThrows(InvalidRequestException.class, () -> service.findByEmail(null));
        Mockito.when(customerRepository.findByEmail("mail@mail.mail")).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> service.findByEmail("mail@mail.mail"));
    }

    @Test
    public void findByNameTest() {
        Mockito.when(customerRepository.findByName("I", "Iva", "ov")).thenReturn(customers);
        List<CustomerDto> result = service.findByName("I", "Iva", "ov");
        assertEquals(2, result.size());
        assertTrue(result.contains(CUSTOMER_DTO_1));
        assertTrue(result.contains(CUSTOMER_DTO_2));
    }

    @Test
    public void findByNameExceptionTest() {
        Mockito.when(customerRepository.findByName("I", "Iva", "ov")).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> service.findByName("I", "Iva", "ov"));
    }

    @Test
    public void saveTest() {
        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(CUSTOMER_1));
        Mockito.when(customerRepository.findByEmail("test@test.test")).thenReturn(Optional.of(CUSTOMER_1));
        Mockito.when(customerRepository.saveAndFlush(CUSTOMER_1)).thenReturn(CUSTOMER_1);
        CustomerDto customerDto = new CustomerDto(1L,
                "Petr", null, "Petrov",
                "test@test.test");
        CustomerDto result = service.save(customerDto);
        assertEquals(customerDto, result);
    }

    @Test
    public void saveNewTest() {
        Mockito.when(customerRepository.findByEmail("")).thenReturn(Optional.empty());
        Customer customer = new Customer();
        BeanUtils.copyProperties(CUSTOMER_2, customer, "id");
        Mockito.when(customerRepository.saveAndFlush(customer)).thenReturn(CUSTOMER_2);
        CustomerDto customerDto = new CustomerDto(null, "Petr", null, "Petrov",
                "test2@test.test");
        assertEquals(CUSTOMER_DTO_2, service.save(customerDto));
    }

    @Test
    public void saveExceptionTest() {
        Mockito.when(customerRepository.findByEmail("test@test.test")).thenReturn(Optional.of(CUSTOMER_1));
        CustomerDto customerDto = new CustomerDto(null, "Petr", null, "Petrov",
                "test@test.test");
        assertThrows(InvalidRequestException.class, () -> service.save(customerDto));
    }

    @Test
    public void removeTest() {
        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(CUSTOMER_1));
        List<Order> orders = new ArrayList<>();
        orders.add(ORDER);
        Mockito.when(orderRepository.findByCustomerId(1L)).thenReturn(orders);
        ORDER.setCancel(true);
        Mockito.when(orderRepository.findById(1L)).thenReturn(Optional.of(ORDER));
        service.removeById(1L);
    }
}
