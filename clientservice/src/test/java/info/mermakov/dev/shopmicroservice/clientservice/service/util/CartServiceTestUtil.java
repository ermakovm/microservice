package info.mermakov.dev.shopmicroservice.clientservice.service.util;

import info.mermakov.dev.shopmicroservice.clientservice.dto.CartDto;
import info.mermakov.dev.shopmicroservice.clientservice.model.Cart;

import static info.mermakov.dev.shopmicroservice.clientservice.service.util.CustomerServiceTestUtil.CUSTOMER_1;

public class CartServiceTestUtil {
    public static final Cart CART_1;
    public static final Cart CART_2;
    public static final CartDto CART_DTO_1;
    public static final CartDto CART_DTO_2;

    static {
        CART_1 = new Cart();
        CART_2 = new Cart();
        CART_DTO_1 = new CartDto();
        CART_DTO_2 = new CartDto();
        initData();
    }

    public static void initData() {
        CART_1.setId(1L);
        CART_1.setCustomer(CUSTOMER_1);
        CART_1.setProductId(1L);
        CART_1.setQuantity(2);

        CART_2.setId(2L);
        CART_2.setCustomer(CUSTOMER_1);
        CART_2.setProductId(2L);
        CART_2.setQuantity(20);

        CART_DTO_1.setId(1L);
        CART_DTO_1.setProductId(1L);
        CART_DTO_1.setQuantity(2);

        CART_DTO_2.setId(2L);
        CART_DTO_2.setProductId(2L);
        CART_DTO_2.setQuantity(20);
    }
}
