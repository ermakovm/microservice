package info.mermakov.dev.shopmicroservice.clientservice.service.util;

import info.mermakov.dev.shopmicroservice.clientservice.model.Order;
import info.mermakov.dev.shopmicroservice.clientservice.model.OrderProduct;

import java.time.LocalDate;

import static info.mermakov.dev.shopmicroservice.clientservice.service.util.CustomerServiceTestUtil.CUSTOMER_1;

public class OrderServiceTestUtil {
    public static final Order ORDER;
    public static final OrderProduct ORDER_PRODUCT_1;
    public static final OrderProduct ORDER_PRODUCT_2;

    static {
        ORDER = new Order();
        ORDER_PRODUCT_1 = new OrderProduct();
        ORDER_PRODUCT_2 = new OrderProduct();

        initData();
    }

    public static void initData() {
        ORDER.setId(1L);
        ORDER.setCustomer(CUSTOMER_1);
        ORDER.setCreateDate(LocalDate.now());
        ORDER.setPrice(100d);
        ORDER.setCancel(false);
        ORDER.setCompleted(false);

        ORDER_PRODUCT_1.setId(1L);
        ORDER_PRODUCT_1.setOrder(ORDER);
        ORDER_PRODUCT_1.setProductId(1L);
        ORDER_PRODUCT_1.setQuantity(2);
        ORDER_PRODUCT_1.setPrice(80d);

        ORDER_PRODUCT_2.setId(2L);
        ORDER_PRODUCT_2.setOrder(ORDER);
        ORDER_PRODUCT_2.setProductId(1L);
        ORDER_PRODUCT_2.setQuantity(20);
        ORDER_PRODUCT_2.setPrice(20d);
    }
}
