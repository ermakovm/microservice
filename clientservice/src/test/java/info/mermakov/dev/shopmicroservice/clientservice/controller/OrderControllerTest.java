package info.mermakov.dev.shopmicroservice.clientservice.controller;

import info.mermakov.dev.shopmicroservice.clientservice.dto.FullOrderDto;
import info.mermakov.dev.shopmicroservice.clientservice.dto.OrderProductDto;
import info.mermakov.dev.shopmicroservice.clientservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.clientservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.clientservice.service.OrderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(OrderController.class)
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application.properties")
public class OrderControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private OrderService service;

    @BeforeEach
    public void init() {
        List<OrderProductDto> products = new ArrayList<>();
        OrderProductDto orderProductDto = new OrderProductDto();
        orderProductDto.setProductId(1L);
        orderProductDto.setPrice(100d);
        orderProductDto.setQuantity(1);
        products.add(orderProductDto);

        FullOrderDto orderDto = new FullOrderDto();
        orderDto.setId(1L);
        orderDto.setCreateDate(LocalDate.now());
        orderDto.setPrice(100d);
        orderDto.setCancel(true);
        orderDto.setProducts(products);

        Mockito.when(service.findById(any())).thenReturn(orderDto);
        Mockito.when(service.save(any())).thenReturn(orderDto);
    }

    @Test
    public void findByIdTest() throws Exception {
        mvc.perform(get("/api/client/orders/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.price").isNotEmpty());
    }

    @Test
    public void saveTest() throws Exception {
        mvc.perform(post("/api/client/orders/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.price").isNotEmpty());
    }

    @Test
    public void updateAndDeleteTest() throws Exception {
        mvc.perform(patch("/api/client/orders/1"))
                .andDo(print())
                .andExpect(status().isOk());

        mvc.perform(delete("/api/client/orders/1"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void exceptionTest() throws Exception {
        Mockito.when(service.findById(any())).thenThrow(ResourceNotFoundException.class);
        mvc.perform(get("/api/client/orders/1"))
                .andDo(print())
                .andExpect(status().isNotFound());

        Mockito.when(service.save(any())).thenThrow(InvalidRequestException.class);
        mvc.perform(post("/api/client/orders/1"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
}
