package info.mermakov.dev.shopmicroservice.clientservice.controller;

import info.mermakov.dev.shopmicroservice.clientservice.dto.CartDto;
import info.mermakov.dev.shopmicroservice.clientservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.clientservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.clientservice.service.CartService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static info.mermakov.dev.shopmicroservice.clientservice.service.util.CartServiceTestUtil.CART_DTO_1;
import static info.mermakov.dev.shopmicroservice.clientservice.service.util.CartServiceTestUtil.CART_DTO_2;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CartController.class)
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application.properties")
public class CartControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private CartService service;

    @BeforeEach
    public void init() {
        List<CartDto> carts = new ArrayList<>();
        carts.add(CART_DTO_1);
        carts.add(CART_DTO_2);
        Mockito.when(service.findByCustomerId(1L)).thenReturn(carts);
    }

    @Test
    public void findAllByIdTest() throws Exception {
        mvc.perform(get("/api/client/carts/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].productId").isNotEmpty());
    }

    @Test
    public void addProductTest() throws Exception {
        mvc.perform(post("/api/client/carts/1/1/1"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void deleteTest() throws Exception {
        mvc.perform(delete("/api/client/carts/1/1"))
                .andDo(print())
                .andExpect(status().isOk());

        mvc.perform(delete("/api/client/carts/1"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void exceptionTest() throws Exception {
        Mockito.when(service.findByCustomerId(1L)).thenThrow(ResourceNotFoundException.class);
        mvc.perform(get("/api/client/carts/1"))
                .andDo(print())
                .andExpect(status().isNotFound());

        Mockito.when(service.findByCustomerId(2L)).thenThrow(InvalidRequestException.class);
        mvc.perform(get("/api/client/carts/2"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
}
