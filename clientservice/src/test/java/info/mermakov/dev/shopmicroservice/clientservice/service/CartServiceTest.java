package info.mermakov.dev.shopmicroservice.clientservice.service;

import info.mermakov.dev.shopmicroservice.clientservice.dto.CartDto;
import info.mermakov.dev.shopmicroservice.clientservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.clientservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.clientservice.model.Cart;
import info.mermakov.dev.shopmicroservice.clientservice.service.util.CartServiceTestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static info.mermakov.dev.shopmicroservice.clientservice.service.util.CartServiceTestUtil.CART_1;
import static info.mermakov.dev.shopmicroservice.clientservice.service.util.CartServiceTestUtil.CART_2;
import static info.mermakov.dev.shopmicroservice.clientservice.service.util.CartServiceTestUtil.CART_DTO_1;
import static info.mermakov.dev.shopmicroservice.clientservice.service.util.CartServiceTestUtil.CART_DTO_2;
import static info.mermakov.dev.shopmicroservice.clientservice.service.util.CustomerServiceTestUtil.CUSTOMER_1;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application.properties")
public class CartServiceTest extends ServiceTest {
    @Autowired
    private CartService service;

    private List<Cart> carts;

    @BeforeEach
    public void init() {
        CartServiceTestUtil.initData();

        carts = new ArrayList<>();
        carts.add(CART_1);
        carts.add(CART_2);
    }

    @Test
    public void findByCustomerIdTest() {
        Mockito.when(cartRepository.findByCustomerId(1L)).thenReturn(carts);
        List<CartDto> result = service.findByCustomerId(1L);
        assertEquals(2, result.size());
        assertTrue(result.contains(CART_DTO_1));
        assertTrue(result.contains(CART_DTO_2));
    }

    @Test
    public void findByCustomerIdExceptionTest() {
        Mockito.when(cartRepository.findByCustomerId(1L)).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> service.findByCustomerId(1L));
    }

    @Test
    public void deleteTest() {
        service.deleteByCustomerAndProductId(1L, 2L);
        service.deleteByCustomerId(1L);
    }

    @Test
    public void saveTest() {
        Mockito.when(cartRepository.findByIdAndProductId(1L, 1L)).thenReturn(Optional.of(CART_1));
        Mockito.when(cartRepository.findByIdAndProductId(1L, 3L)).thenReturn(Optional.empty());
        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(CUSTOMER_1));
        service.save(1L, 1L, 222);
        service.save(1L, 3L, 112);
    }

    @Test
    public void saveExceptionTest() {
        assertThrows(InvalidRequestException.class, () -> service.save(1L, 1L, 0));
        assertThrows(InvalidRequestException.class, () -> service.save(1L, -15L, 10));
        assertThrows(InvalidRequestException.class, () -> service.save(-1L, 1L, 10));
    }
}
