package info.mermakov.dev.shopmicroservice.clientservice.controller;

import info.mermakov.dev.shopmicroservice.clientservice.dto.CustomerDto;
import info.mermakov.dev.shopmicroservice.clientservice.dto.OrderDto;
import info.mermakov.dev.shopmicroservice.clientservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.clientservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.clientservice.service.CustomerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static info.mermakov.dev.shopmicroservice.clientservice.service.util.CustomerServiceTestUtil.CUSTOMER_DTO_1;
import static info.mermakov.dev.shopmicroservice.clientservice.service.util.CustomerServiceTestUtil.CUSTOMER_DTO_2;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CustomerController.class)
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application.properties")
public class CustomerControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private CustomerService service;

    @BeforeEach
    public void init() {
        List<CustomerDto> customers = new ArrayList<>();
        customers.add(CUSTOMER_DTO_1);
        customers.add(CUSTOMER_DTO_2);
        Mockito.when(service.findAll()).thenReturn(customers);
        Mockito.when(service.findByName(any(), any(), any())).thenReturn(customers);
    }

    @Test
    public void findCustomersTest() throws Exception {
        mvc.perform(get("/api/client/customers/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].firstName").isNotEmpty());

        mvc.perform(get("/api/client/customers/")
                .content("{\"firstName\":\"Y\",\"middleName\":\"\",\"lastName\":\"las\"}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].firstName").isNotEmpty());
    }

    @Test
    public void findByEmailTest() throws Exception {
        Mockito.when(service.findByEmail(any())).thenReturn(CUSTOMER_DTO_1);
        mvc.perform(get("/api/client/customers/email")
                .content("{\"email\":\"test@test.test\"}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").isNotEmpty());
    }

    @Test
    public void findByIdTest() throws Exception {
        Mockito.when(service.findById(any())).thenReturn(CUSTOMER_DTO_1);
        mvc.perform(get("/api/client/customers/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").isNotEmpty());
    }

    @Test
    public void findOrdersTest() throws Exception {
        List<OrderDto> result = new ArrayList<>();
        OrderDto orderDto = new OrderDto();
        orderDto.setId(1L);
        orderDto.setPrice(100d);
        orderDto.setCreateDate(LocalDate.now());
        orderDto.setCompletionDate(LocalDate.now());
        result.add(orderDto);
        Mockito.when(service.findOrdersByCustomerId(any())).thenReturn(result);

        mvc.perform(get("/api/client/customers/1/orders"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].price").isNotEmpty());
    }

    @Test
    public void saveTest() throws Exception {
        Mockito.when(service.save(any())).thenReturn(CUSTOMER_DTO_1);
        mvc.perform(post("/api/client/customers")
                .content("{\"firstName\":\"Ivan\",\"middleName\":\"Ivanovich\",\"lastName\":\"Ivanov\",\"email\":\"test@test.test\"}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").isNotEmpty());

        mvc.perform(put("/api/client/customers/1")
                .content("{\"firstName\":\"Ivan\",\"middleName\":\"Ivanovich\",\"lastName\":\"Ivanov\",\"email\":\"test@test.test\"}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").isNotEmpty());
    }

    @Test
    public void deleteTest() throws Exception {
        mvc.perform(delete("/api/client/customers/1"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void exceptionTest() throws Exception {
        Mockito.when(service.findAll()).thenThrow(ResourceNotFoundException.class);
        Mockito.when(service.save(any())).thenThrow(InvalidRequestException.class);
        mvc.perform(get("/api/client/customers"))
                .andDo(print())
                .andExpect(status().isNotFound());

        mvc.perform(post("/api/client/customers")
                .content("{\"firstName\":\"Ivan\",\"middleName\":\"Ivanovich\",\"lastName\":\"Ivanov\",\"email\":\"test@test.test\"}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
}
