package info.mermakov.dev.shopmicroservice.clientservice.service;

import info.mermakov.dev.shopmicroservice.clientservice.dto.FullOrderDto;
import info.mermakov.dev.shopmicroservice.clientservice.dto.OrderDto;
import info.mermakov.dev.shopmicroservice.clientservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.clientservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.clientservice.model.Cart;
import info.mermakov.dev.shopmicroservice.clientservice.model.Order;
import info.mermakov.dev.shopmicroservice.clientservice.model.OrderProduct;
import info.mermakov.dev.shopmicroservice.clientservice.service.util.CartServiceTestUtil;
import info.mermakov.dev.shopmicroservice.clientservice.service.util.CustomerServiceTestUtil;
import info.mermakov.dev.shopmicroservice.clientservice.service.util.OrderServiceTestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static info.mermakov.dev.shopmicroservice.clientservice.service.util.CartServiceTestUtil.CART_1;
import static info.mermakov.dev.shopmicroservice.clientservice.service.util.CartServiceTestUtil.CART_2;
import static info.mermakov.dev.shopmicroservice.clientservice.service.util.CustomerServiceTestUtil.CUSTOMER_1;
import static info.mermakov.dev.shopmicroservice.clientservice.service.util.OrderServiceTestUtil.ORDER;
import static info.mermakov.dev.shopmicroservice.clientservice.service.util.OrderServiceTestUtil.ORDER_PRODUCT_1;
import static info.mermakov.dev.shopmicroservice.clientservice.service.util.OrderServiceTestUtil.ORDER_PRODUCT_2;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application.properties")
public class OrderServiceTest extends ServiceTest {
    @Autowired
    private OrderService service;

    @BeforeEach
    public void init() {
        OrderServiceTestUtil.initData();
        CartServiceTestUtil.initData();
        CustomerServiceTestUtil.initData();
    }

    @Test
    public void findBYCustomerId() {
        List<Order> orders = new ArrayList<>();
        orders.add(ORDER);
        Mockito.when(orderRepository.findByCustomerId(1L)).thenReturn(orders);
        List<OrderDto> result = service.findByCustomerId(1L);
        assertEquals(1, result.size());
        assertEquals(100d, result.get(0).getPrice());
    }

    @Test
    public void findByCustomerIdExceptionTest() {
        Mockito.when(orderRepository.findByCustomerId(1L)).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> service.findByCustomerId(1L));
    }

    @Test
    public void findByIdTest() {
        Mockito.when(orderRepository.findById(1L)).thenReturn(Optional.of(ORDER));
        List<OrderProduct> products = new ArrayList<>();
        products.add(ORDER_PRODUCT_1);
        products.add(ORDER_PRODUCT_2);
        Mockito.when(productRepository.findProducts(1L)).thenReturn(products);
        FullOrderDto result = service.findById(1L);
        assertEquals(1L, result.getId());
        assertEquals(2, result.getProducts().size());
    }

    @Test
    public void findByIdExceptionTest() {
        Mockito.when(orderRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> service.findById(1L));
    }

    @Test
    public void saveTest() {
        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(CUSTOMER_1));
        List<Cart> carts = new ArrayList<>();
        carts.add(CART_1);
        carts.add(CART_2);
        Mockito.when(cartRepository.findByCustomerId(1L)).thenReturn(carts);
        Mockito.when(client.checkProduct(any())).thenReturn(true);
        Mockito.when(client.getPrice(any())).thenReturn(1d);
        Mockito.when(orderRepository.saveAndFlush(any())).thenReturn(ORDER);
        Mockito.when(productRepository.saveAndFlush(any())).thenReturn(ORDER_PRODUCT_1);

        FullOrderDto result = service.save(1L);
        assertEquals(1L, result.getId());
        assertEquals(2, result.getProducts().size());
    }

    @Test
    public void saveExceptionTest() {
        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(CUSTOMER_1));
        List<Cart> carts = new ArrayList<>();
        carts.add(CART_1);
        carts.add(CART_2);
        Mockito.when(cartRepository.findByCustomerId(1L)).thenReturn(carts);
        Mockito.when(client.checkProduct(any())).thenReturn(false);

        assertThrows(InvalidRequestException.class, () -> service.save(1L));
    }

    @Test
    public void setCompleteTest() {
        Mockito.when(orderRepository.findById(1L)).thenReturn(Optional.of(ORDER));
        service.setComplete(1L);
    }

    @Test
    public void setCancelTest() {
        Mockito.when(orderRepository.findById(1L)).thenReturn(Optional.of(ORDER));
        service.setCancel(1L);
    }

    @Test
    public void delete1Test() {
        Mockito.when(orderRepository.findById(1L)).thenReturn(Optional.of(ORDER));
        ORDER.setCancel(true);
        service.deleteOrderById(1L);
    }

    @Test
    public void delete2Test() {
        Mockito.when(orderRepository.findById(1L)).thenReturn(Optional.of(ORDER));
        ORDER.setCompleted(true);
        List<OrderProduct> products = new ArrayList<>();
        products.add(ORDER_PRODUCT_1);
        products.add(ORDER_PRODUCT_2);
        Mockito.when(productRepository.findProducts(1L)).thenReturn(products);
        service.deleteOrderById(1L);
    }

    @Test
    public void deleteExceptionTest() {
        Mockito.when(orderRepository.findById(1L)).thenReturn(Optional.of(ORDER));
        assertThrows(InvalidRequestException.class, () -> service.deleteOrderById(1L));
    }
}
