package info.mermakov.dev.shopmicroservice.clientservice.service;

import info.mermakov.dev.shopmicroservice.clientservice.repository.CartRepository;
import info.mermakov.dev.shopmicroservice.clientservice.repository.CustomerRepository;
import info.mermakov.dev.shopmicroservice.clientservice.repository.OrderProductRepository;
import info.mermakov.dev.shopmicroservice.clientservice.repository.OrderRepository;
import org.springframework.boot.test.mock.mockito.MockBean;

public abstract class ServiceTest {
    @MockBean
    protected QueueListener listener;

    @MockBean
    protected QueueSender sender;

    @MockBean
    protected FeignClient client;

    @MockBean
    protected CartRepository cartRepository;

    @MockBean
    protected CustomerRepository customerRepository;

    @MockBean
    protected OrderProductRepository productRepository;

    @MockBean
    protected OrderRepository orderRepository;
}
