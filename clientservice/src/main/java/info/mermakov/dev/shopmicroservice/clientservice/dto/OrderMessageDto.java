package info.mermakov.dev.shopmicroservice.clientservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderMessageDto implements Serializable {
    @Positive
    private Long orderId;

    @NotNull
    private List<Pair> products;

    private Boolean returnProduct;
}
