package info.mermakov.dev.shopmicroservice.clientservice.repository;

import info.mermakov.dev.shopmicroservice.clientservice.model.OrderProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OrderProductRepository extends JpaRepository<OrderProduct, Long> {
    @Query("from OrderProduct where order.id = :id")
    List<OrderProduct> findProducts(@Param("id") Long id);

    void deleteByOrderId(Long id);
}
