package info.mermakov.dev.shopmicroservice.clientservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FullOrderDto {
    private Long id;
    private LocalDate createDate;
    private Double price;
    private LocalDate completionDate;
    private Boolean cancel;
    private List<OrderProductDto> products;
}
