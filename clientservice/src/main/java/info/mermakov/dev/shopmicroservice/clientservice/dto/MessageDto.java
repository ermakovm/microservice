package info.mermakov.dev.shopmicroservice.clientservice.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class MessageDto implements Serializable {
    private String email;
    private String subject;
    private String text;
}
