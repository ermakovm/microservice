package info.mermakov.dev.shopmicroservice.clientservice.config;

import lombok.Getter;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("!test")
public class RabbitConfig {
    @Getter
    @Value("${project.queue.order-queue}")
    private String orderQueue;

    @Getter
    @Value("${project.queue.status-queue}")
    private String statusQueue;

    @Getter
    @Value("${project.queue.mail-queue}")
    private String mailQueue;

    @Bean
    public Queue orderQueue() {
        return new Queue(orderQueue, true, false, false);
    }

    @Bean
    public Queue statusQueue() {
        return new Queue(statusQueue, true, false, false);
    }

    @Bean
    public Queue mailQueue() {
        return new Queue(mailQueue, true, false, false);
    }

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
