package info.mermakov.dev.shopmicroservice.clientservice.service.impl;

import info.mermakov.dev.shopmicroservice.clientservice.dto.CartDto;
import info.mermakov.dev.shopmicroservice.clientservice.dto.FullOrderDto;
import info.mermakov.dev.shopmicroservice.clientservice.dto.MessageDto;
import info.mermakov.dev.shopmicroservice.clientservice.dto.OrderDto;
import info.mermakov.dev.shopmicroservice.clientservice.dto.OrderMessageDto;
import info.mermakov.dev.shopmicroservice.clientservice.dto.OrderProductDto;
import info.mermakov.dev.shopmicroservice.clientservice.dto.Pair;
import info.mermakov.dev.shopmicroservice.clientservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.clientservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.clientservice.model.Customer;
import info.mermakov.dev.shopmicroservice.clientservice.model.Order;
import info.mermakov.dev.shopmicroservice.clientservice.model.OrderProduct;
import info.mermakov.dev.shopmicroservice.clientservice.repository.OrderProductRepository;
import info.mermakov.dev.shopmicroservice.clientservice.repository.OrderRepository;
import info.mermakov.dev.shopmicroservice.clientservice.service.CartService;
import info.mermakov.dev.shopmicroservice.clientservice.service.CustomerService;
import info.mermakov.dev.shopmicroservice.clientservice.service.FeignClient;
import info.mermakov.dev.shopmicroservice.clientservice.service.OrderService;
import info.mermakov.dev.shopmicroservice.clientservice.service.QueueSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static info.mermakov.dev.shopmicroservice.clientservice.exception.ErrorMessage.DELETE_ERROR;
import static info.mermakov.dev.shopmicroservice.clientservice.exception.ErrorMessage.NO_FOUND_BY_ID_ERROR;
import static info.mermakov.dev.shopmicroservice.clientservice.exception.ErrorMessage.NO_FOUND_ERROR;
import static info.mermakov.dev.shopmicroservice.clientservice.exception.ErrorMessage.PRODUCT_HIDE;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {
    private final OrderRepository repository;
    private final OrderProductRepository orderProductRepository;
    private final QueueSender queueSender;
    private final CustomerService customerService;
    private final CartService cartService;
    private final FeignClient feignClient;

    @Autowired
    public OrderServiceImpl(OrderRepository repository,
                            OrderProductRepository orderProductRepository,
                            QueueSender queueSender,
                            @Lazy CustomerService customerService,
                            @Lazy CartService cartService,
                            FeignClient feignClient) {
        this.repository = repository;
        this.orderProductRepository = orderProductRepository;
        this.queueSender = queueSender;
        this.cartService = cartService;
        this.customerService = customerService;
        this.feignClient = feignClient;
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public List<OrderDto> findByCustomerId(Long id) {
        log.debug("get data from db");
        List<Order> orders = repository.findByCustomerId(id);
        if (orders.size() > 0) {
            List<OrderDto> result = new ArrayList<>();
            for (Order order : orders) {
                OrderDto orderDto = new OrderDto();
                BeanUtils.copyProperties(order, orderDto, "customer", "completed");
                result.add(orderDto);
            }
            return result;
        }
        throw new ResourceNotFoundException(NO_FOUND_ERROR);
    }

    @Override
    public List<Order> findOrdersById(Long id) {
        log.debug("get data from db by customer id");
        return repository.findByCustomerId(1L);
    }

    private Order getById(Long id) {
        log.debug("get order by id");
        Optional<Order> order = repository.findById(id);
        return order.orElseThrow(() -> new ResourceNotFoundException(NO_FOUND_BY_ID_ERROR + id));
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public FullOrderDto findById(Long id) {
        Order order = getById(id);
        log.debug("get data from order_product table");
        List<OrderProduct> products = orderProductRepository.findProducts(id);
        return convertToDto(order, products);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public FullOrderDto save(Long id) {
        log.debug("get data from customer service");
        Customer customer = customerService.getById(id);
        log.debug("get data from cart service");
        List<CartDto> carts = cartService.findByCustomerId(id);
        List<Pair> pair = new ArrayList<>();
        Order order = new Order();
        order.setCustomer(customer);
        order.setCreateDate(LocalDate.now());
        order.setCancel(false);
        order.setCompleted(false);

        List<OrderProduct> products = new ArrayList<>();
        Double cost = 0d;
        for (CartDto cartDto : carts) {
            OrderProduct product = new OrderProduct();
            Long productId = cartDto.getProductId();
            log.debug("check product int catalog service");
            Boolean productAvailable = feignClient.checkProduct(productId);
            if (!productAvailable) {
                throw new InvalidRequestException(PRODUCT_HIDE);
            }
            product.setProductId(productId);
            product.setQuantity(cartDto.getQuantity());
            log.debug("get price from catalog service");
            Double price = feignClient.getPrice(productId);
            product.setPrice(price * cartDto.getQuantity());
            cost += product.getPrice();
            products.add(product);
            pair.add(new Pair(productId, cartDto.getQuantity()));
        }

        order.setPrice(cost);
        log.debug("save order in db");
        Order result = repository.saveAndFlush(order);
        for (OrderProduct orderProduct : products) {
            orderProduct.setOrder(result);
            log.debug("save order_product in db");
            orderProductRepository.saveAndFlush(orderProduct);
        }
        OrderMessageDto messageDto = new OrderMessageDto();
        messageDto.setReturnProduct(false);
        messageDto.setOrderId(result.getId());
        messageDto.setProducts(pair);
        log.debug("send message to queue");
        queueSender.send(messageDto);
        log.debug("delete data from cart");
        cartService.deleteByCustomerId(id);
        return convertToDto(result, products);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public void setComplete(Long id) {
        Order order = getById(id);
        order.setCompletionDate(LocalDate.now());
        log.debug("set completion date");
        repository.saveAndFlush(order);

        MessageDto messageDto = new MessageDto();
        messageDto.setEmail(order.getCustomer().getEmail());
        messageDto.setSubject("Order received");
        messageDto.setText("Order received");
        log.debug("send message to customer");
        queueSender.send(messageDto);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public void setReady(Long id) {
        Order order = getById(id);
        order.setCompleted(true);
        log.debug("set completed flat");
        repository.saveAndFlush(order);

        MessageDto messageDto = new MessageDto();
        messageDto.setEmail(order.getCustomer().getEmail());
        messageDto.setSubject("Order ready");
        messageDto.setText("Order ready");
        log.debug("send message to email");
        queueSender.send(messageDto);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public void setCancel(Long id) {
        Order order = getById(id);
        order.setCancel(true);
        log.debug("set cancel flag");
        repository.saveAndFlush(order);
        MessageDto messageDto = new MessageDto();
        messageDto.setEmail(order.getCustomer().getEmail());
        messageDto.setSubject("Order canceled");
        messageDto.setText("Order canceled");
        log.debug("send message to email");
        queueSender.send(messageDto);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public void deleteOrderById(Long id) {
        Order order = getById(id);
        if (order.getCompletionDate() != null || order.getCancel()) {
            log.debug("Simple delete order");
            orderProductRepository.deleteByOrderId(id);
            repository.delete(order);
            return;
        }
        if (!order.getCompleted()) {
            throw new InvalidRequestException(DELETE_ERROR);
        } else {
            log.debug("get data from order_product");
            List<OrderProduct> products = orderProductRepository.findProducts(id);
            OrderMessageDto messageDto = new OrderMessageDto();
            messageDto.setOrderId(id);
            messageDto.setReturnProduct(true);
            List<Pair> pairs = new ArrayList<>();
            for (OrderProduct product : products) {
                Pair pair = new Pair(product.getProductId(), -1 * product.getQuantity());
                pairs.add(pair);
            }
            messageDto.setProducts(pairs);
            log.debug("send message with order to return");
            queueSender.send(messageDto);
            log.debug("delete from order_product");
            orderProductRepository.deleteByOrderId(id);
            log.debug("delete order");
            repository.delete(order);
        }
    }

    private FullOrderDto convertToDto(Order order, List<OrderProduct> products) {
        FullOrderDto orderDto = new FullOrderDto();
        BeanUtils.copyProperties(order, orderDto, "customer", "products");
        List<OrderProductDto> productsDto = new ArrayList<>();
        for (OrderProduct orderProduct : products) {
            OrderProductDto productDto = new OrderProductDto();
            BeanUtils.copyProperties(orderProduct, productDto, "order", "id");
            productsDto.add(productDto);
        }
        orderDto.setProducts(productsDto);
        return orderDto;
    }
}
