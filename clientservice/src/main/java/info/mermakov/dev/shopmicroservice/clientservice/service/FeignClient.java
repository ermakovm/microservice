package info.mermakov.dev.shopmicroservice.clientservice.service;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@org.springframework.cloud.openfeign.FeignClient("CATALOG-SERVICE")
public interface FeignClient {
    @GetMapping("/api/catalog/products/{id}/valid")
    Boolean checkProduct(@PathVariable Long id);

    @GetMapping("/api/catalog/products/{id}/price")
    Double getPrice(@PathVariable Long id);
}
