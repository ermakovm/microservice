package info.mermakov.dev.shopmicroservice.clientservice.controller;

import info.mermakov.dev.shopmicroservice.clientservice.dto.CustomerDto;
import info.mermakov.dev.shopmicroservice.clientservice.dto.OrderDto;
import info.mermakov.dev.shopmicroservice.clientservice.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/api/client/customers")
public class CustomerController {
    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping
    public List<CustomerDto> findCustomers(@RequestBody(required = false) CustomerDto customerDto) {
        log.debug("find customers");
        if (customerDto == null) {
            return customerService.findAll();
        }
        String firstName = customerDto.getFirstName();
        String middleName = customerDto.getMiddleName();
        String lastName = customerDto.getLastName();
        return customerService.findByName(firstName, middleName, lastName);
    }

    @GetMapping("/email")
    public CustomerDto findByEmail(@RequestBody CustomerDto customerDto) {
        log.debug("find by email");
        return customerService.findByEmail(customerDto.getEmail());
    }

    @GetMapping("/{id}")
    public CustomerDto findById(@PathVariable Long id) {
        log.debug("find by id");
        return customerService.findById(id);
    }

    @GetMapping("/{id}/orders")
    public List<OrderDto> findOrdersByCustomerId(@PathVariable Long id) {
        log.debug("get orders by customer id");
        return customerService.findOrdersByCustomerId(id);
    }

    @PostMapping
    public CustomerDto saveCustomer(@Validated @RequestBody CustomerDto customerDto) {
        log.debug("create new customer");
        customerDto.setId(null);
        return customerService.save(customerDto);
    }

    @PutMapping("/{id}")
    public CustomerDto saveCustomer(@Validated @RequestBody CustomerDto customerDto, @PathVariable Long id) {
        log.debug("update customer");
        customerDto.setId(id);
        return customerService.save(customerDto);
    }

    @DeleteMapping("/{id}")
    public void deleteCustomer(@PathVariable Long id) {
        log.debug("delete customer");
        customerService.removeById(id);
    }
}
