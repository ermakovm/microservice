package info.mermakov.dev.shopmicroservice.clientservice.service.impl;

import info.mermakov.dev.shopmicroservice.clientservice.dto.CustomerDto;
import info.mermakov.dev.shopmicroservice.clientservice.dto.MessageDto;
import info.mermakov.dev.shopmicroservice.clientservice.dto.OrderDto;
import info.mermakov.dev.shopmicroservice.clientservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.clientservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.clientservice.model.Customer;
import info.mermakov.dev.shopmicroservice.clientservice.model.Order;
import info.mermakov.dev.shopmicroservice.clientservice.repository.CustomerRepository;
import info.mermakov.dev.shopmicroservice.clientservice.service.CartService;
import info.mermakov.dev.shopmicroservice.clientservice.service.CustomerService;
import info.mermakov.dev.shopmicroservice.clientservice.service.OrderService;
import info.mermakov.dev.shopmicroservice.clientservice.service.QueueSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static info.mermakov.dev.shopmicroservice.clientservice.exception.ErrorMessage.EMAIL_ALREADY_EXIST_ERROR;
import static info.mermakov.dev.shopmicroservice.clientservice.exception.ErrorMessage.FIELD_INVALID_ERROR;
import static info.mermakov.dev.shopmicroservice.clientservice.exception.ErrorMessage.NO_FOUND_BY_ID_ERROR;
import static info.mermakov.dev.shopmicroservice.clientservice.exception.ErrorMessage.NO_FOUND_ERROR;

@Slf4j
@Service
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository repository;
    private final QueueSender queueSender;
    private final CartService cartService;
    private final OrderService orderService;

    @Autowired
    public CustomerServiceImpl(CustomerRepository repository,
                               QueueSender queueSender,
                               CartService cartService,
                               OrderService orderService) {
        this.repository = repository;
        this.queueSender = queueSender;
        this.cartService = cartService;
        this.orderService = orderService;
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public List<CustomerDto> findAll() {
        log.debug("get data from db");
        List<Customer> customers = repository.findAll();
        if (customers.size() > 0) {
            return convertToDto(customers);
        }
        throw new ResourceNotFoundException(NO_FOUND_ERROR);
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public List<CustomerDto> findByName(String firstName, String middleName, String lastName) {
        log.debug("get data from debug");
        List<Customer> customers = repository.findByName(firstName, middleName, lastName);
        if (customers.size() > 0) {
            return convertToDto(customers);
        }
        throw new ResourceNotFoundException(NO_FOUND_ERROR);
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public Customer getById(Long id) {
        log.debug("get data by id");
        Optional<Customer> customer = repository.findById(id);
        return customer.orElseThrow(() -> new ResourceNotFoundException(NO_FOUND_BY_ID_ERROR + id));
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public CustomerDto findById(Long id) {
        return convertToDto(getById(id));
    }

    private Customer getCustomerOrNullByEmail(String email) {
        log.debug("get data by email");
        Optional<Customer> customer = repository.findByEmail(email);
        return customer.orElse(null);
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public CustomerDto findByEmail(String email) {
        if (email == null) {
            throw new InvalidRequestException(FIELD_INVALID_ERROR + "'email'");
        }
        Customer customer = getCustomerOrNullByEmail(email);
        if (customer != null) {
            return convertToDto(customer);
        }
        throw new ResourceNotFoundException(NO_FOUND_ERROR + email);
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public List<OrderDto> findOrdersByCustomerId(Long id) {
        log.debug("get data from order service");
        return orderService.findByCustomerId(id);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public CustomerDto save(@Validated CustomerDto customerDto) {
        boolean isNew = false;
        Customer customer;
        if (customerDto.getId() != null) {
            customer = getById(customerDto.getId());
        } else {
            customer = new Customer();
            isNew = true;
        }
        Customer customerByEmail = getCustomerOrNullByEmail(customerDto.getEmail());
        if (customerByEmail != null && !customerByEmail.getId().equals(customerDto.getId())) {
            throw new InvalidRequestException(EMAIL_ALREADY_EXIST_ERROR);
        }
        BeanUtils.copyProperties(customerDto, customer, "id");
        log.debug("save customer to db");
        Customer newCustomer = repository.saveAndFlush(customer);
        if (isNew) {
            MessageDto messageDto = new MessageDto();
            messageDto.setEmail(newCustomer.getEmail());
            messageDto.setSubject("New User");
            messageDto.setText("You register");
            log.debug("send message to email");
            queueSender.send(messageDto);
        }
        return convertToDto(newCustomer);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public void removeById(Long id) {
        Customer customer = getById(id);
        log.debug("delete from cartService");
        cartService.deleteByCustomerId(id);
        log.debug("get data from order service");
        List<Order> orders = orderService.findOrdersById(id);
        for (Order order : orders) {
            log.debug("delete order by id");
            orderService.deleteOrderById(order.getId());
        }
        log.debug("delete customer from db");
        repository.delete(customer);
    }

    @Override
    public CustomerDto convertToDto(Customer customer) {
        CustomerDto result = new CustomerDto();
        BeanUtils.copyProperties(customer, result);
        return result;
    }

    @Override
    public List<CustomerDto> convertToDto(Iterable<Customer> customers) {
        List<CustomerDto> result = new ArrayList<>();
        for (Customer customer : customers) {
            result.add(convertToDto(customer));
        }
        return result;
    }
}
