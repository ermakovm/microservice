package info.mermakov.dev.shopmicroservice.clientservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderProductDto {
    private Long productId;
    private Integer quantity;
    private Double price;
}
