package info.mermakov.dev.shopmicroservice.clientservice.service;

import com.rabbitmq.client.Channel;
import info.mermakov.dev.shopmicroservice.clientservice.dto.ResultMessageDto;

import java.io.IOException;

public interface QueueListener {
    void receive(ResultMessageDto messageDto, Channel channel, long tag) throws IOException;
}
