package info.mermakov.dev.shopmicroservice.clientservice.service;

import info.mermakov.dev.shopmicroservice.clientservice.dto.FullOrderDto;
import info.mermakov.dev.shopmicroservice.clientservice.dto.OrderDto;
import info.mermakov.dev.shopmicroservice.clientservice.model.Order;

import java.util.List;

public interface OrderService {
    List<OrderDto> findByCustomerId(Long id);

    List<Order> findOrdersById(Long id);

    FullOrderDto findById(Long id);

    FullOrderDto save(Long id);

    void setComplete(Long id);

    void setReady(Long id);

    void setCancel(Long id);

    void deleteOrderById(Long id);
}
