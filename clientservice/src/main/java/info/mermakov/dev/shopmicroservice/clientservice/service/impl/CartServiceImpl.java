package info.mermakov.dev.shopmicroservice.clientservice.service.impl;

import info.mermakov.dev.shopmicroservice.clientservice.dto.CartDto;
import info.mermakov.dev.shopmicroservice.clientservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.clientservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.clientservice.model.Cart;
import info.mermakov.dev.shopmicroservice.clientservice.model.Customer;
import info.mermakov.dev.shopmicroservice.clientservice.repository.CartRepository;
import info.mermakov.dev.shopmicroservice.clientservice.service.CartService;
import info.mermakov.dev.shopmicroservice.clientservice.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static info.mermakov.dev.shopmicroservice.clientservice.exception.ErrorMessage.INVALID_PARAMETERS;
import static info.mermakov.dev.shopmicroservice.clientservice.exception.ErrorMessage.NO_FOUND_ERROR;

@Slf4j
@Service
public class CartServiceImpl implements CartService {
    private final CartRepository repository;
    private final CustomerService customerService;

    @Autowired
    public CartServiceImpl(CartRepository repository, @Lazy CustomerService customerService) {
        this.repository = repository;
        this.customerService = customerService;
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public List<CartDto> findByCustomerId(Long id) {
        log.debug("get data from db");
        List<Cart> carts = repository.findByCustomerId(id);
        if (carts.size() > 0) {
            return convertToDto(carts);
        }
        throw new ResourceNotFoundException(NO_FOUND_ERROR);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public void save(Long id, Long productId, Integer count) {
        if (id <= 0 || productId <= 0 || count <= 0) {
            throw new InvalidRequestException(INVALID_PARAMETERS);
        }
        log.debug("Get data from db");
        Optional<Cart> data = repository.findByIdAndProductId(id, productId);
        Cart cart = data.orElse(new Cart());
        if (cart.getCustomer() == null) {
            log.debug("get customer from service");
            Customer customer = customerService.getById(id);
            cart.setCustomer(customer);
        }
        cart.setProductId(productId);
        cart.setQuantity(count);
        log.debug("save cart");
        repository.saveAndFlush(cart);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public void deleteByCustomerAndProductId(Long id, Long productId) {
        log.debug("delete from db by id and productId");
        repository.deleteByIdAndProduct(id, productId);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public void deleteByCustomerId(Long id) {
        log.debug("delete from db by id");
        repository.deleteByCustomerId(id);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public CartDto convertToDto(Cart cart) {
        CartDto cartDto = new CartDto();
        BeanUtils.copyProperties(cart, cartDto, "customer");
        return cartDto;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public List<CartDto> convertToDto(Iterable<Cart> carts) {
        List<CartDto> result = new ArrayList<>();
        for (Cart cart : carts) {
            result.add(convertToDto(cart));
        }
        return result;
    }
}
