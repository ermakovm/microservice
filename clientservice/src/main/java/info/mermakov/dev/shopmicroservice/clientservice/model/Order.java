package info.mermakov.dev.shopmicroservice.clientservice.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Positive;
import java.time.LocalDate;

@Entity
@Table(name = "order_data")
@Data
@NoArgsConstructor
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;

    @Column(name = "create_date", nullable = false)
    private LocalDate createDate;

    @Column(name = "price", nullable = false)
    @Positive
    private Double price;

    @Column(name = "completion_date")
    private LocalDate completionDate;

    @Column(name = "cancel", nullable = false)
    private Boolean cancel;

    @Column(name = "completed", nullable = false)
    private Boolean completed;
}
