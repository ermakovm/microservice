package info.mermakov.dev.shopmicroservice.clientservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultMessageDto implements Serializable {
    private Long id;
    private Boolean success;
}
