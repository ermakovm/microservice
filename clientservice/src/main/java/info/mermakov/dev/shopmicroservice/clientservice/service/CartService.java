package info.mermakov.dev.shopmicroservice.clientservice.service;

import info.mermakov.dev.shopmicroservice.clientservice.dto.CartDto;
import info.mermakov.dev.shopmicroservice.clientservice.model.Cart;

import java.util.List;

public interface CartService {
    List<CartDto> findByCustomerId(Long id);

    void save(Long id, Long productId, Integer count);

    void deleteByCustomerAndProductId(Long id, Long productId);

    void deleteByCustomerId(Long id);

    CartDto convertToDto(Cart cart);

    List<CartDto> convertToDto(Iterable<Cart> carts);
}
