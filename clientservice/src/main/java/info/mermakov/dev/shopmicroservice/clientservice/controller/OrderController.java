package info.mermakov.dev.shopmicroservice.clientservice.controller;

import info.mermakov.dev.shopmicroservice.clientservice.dto.FullOrderDto;
import info.mermakov.dev.shopmicroservice.clientservice.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(value = "/api/client/orders")
public class OrderController {
    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/{id}")
    public FullOrderDto getOrderById(@PathVariable Long id) {
        log.debug("get order by id");
        return orderService.findById(id);
    }

    @PostMapping("/{id}")
    public FullOrderDto saveOrder(@PathVariable Long id) {
        log.debug("save order by customer cart");
        return orderService.save(id);
    }

    @PatchMapping("/{id}")
    public void setComplete(@PathVariable Long id) {
        log.debug("set completion date to order");
        orderService.setComplete(id);
    }

    @DeleteMapping("/{id}")
    public void deleteOrderById(@PathVariable Long id) {
        log.debug("delete order");
        orderService.deleteOrderById(id);
    }
}
