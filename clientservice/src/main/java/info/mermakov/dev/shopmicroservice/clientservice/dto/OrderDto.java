package info.mermakov.dev.shopmicroservice.clientservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {
    private Long id;
    private LocalDate createDate;
    private Double price;
    private LocalDate completionDate;
    private Boolean cancel;
}
