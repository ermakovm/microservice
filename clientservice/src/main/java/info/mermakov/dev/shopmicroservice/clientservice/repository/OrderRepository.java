package info.mermakov.dev.shopmicroservice.clientservice.repository;

import info.mermakov.dev.shopmicroservice.clientservice.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findByCustomerId(Long id);
}
