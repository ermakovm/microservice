package info.mermakov.dev.shopmicroservice.clientservice.repository;

import info.mermakov.dev.shopmicroservice.clientservice.model.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CartRepository extends JpaRepository<Cart, Long> {
    List<Cart> findByCustomerId(Long id);

    void deleteByCustomerId(Long id);

    @Modifying
    @Query("delete from Cart where customer.id = :id and productId = :pId")
    void deleteByIdAndProduct(@Param("id") Long id, @Param("pId") Long pId);

    @Query("from Cart where customer.id = :id and productId = :pId")
    Optional<Cart> findByIdAndProductId(@Param("id") Long id, @Param("pId") Long pId);
}
