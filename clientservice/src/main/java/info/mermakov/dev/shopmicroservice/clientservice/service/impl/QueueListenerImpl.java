package info.mermakov.dev.shopmicroservice.clientservice.service.impl;

import com.rabbitmq.client.Channel;
import info.mermakov.dev.shopmicroservice.clientservice.dto.ResultMessageDto;
import info.mermakov.dev.shopmicroservice.clientservice.service.OrderService;
import info.mermakov.dev.shopmicroservice.clientservice.service.QueueListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Slf4j
@Service
public class QueueListenerImpl implements QueueListener {
    private final OrderService service;

    @Autowired
    public QueueListenerImpl(OrderService service) {
        this.service = service;
    }

    @RabbitListener(queues = "${project.queue.status-queue}", ackMode = "MANUAL")
    @Override
    public void receive(ResultMessageDto messageDto, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws IOException {
        log.debug("Get message with result");
        Long id = messageDto.getId();
        Boolean result = messageDto.getSuccess();
        if (result) {
            log.debug("set ready");
            service.setReady(id);
        } else {
            log.debug("set cancel");
            service.setCancel(id);
        }
        log.debug("set tag");
        channel.basicAck(tag, false);
    }
}
