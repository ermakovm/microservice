package info.mermakov.dev.shopmicroservice.clientservice.controller;

import info.mermakov.dev.shopmicroservice.clientservice.dto.CartDto;
import info.mermakov.dev.shopmicroservice.clientservice.service.CartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/api/client/carts")
public class CartController {
    private final CartService cartService;

    @Autowired
    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @GetMapping("/{id}")
    public List<CartDto> findCartByCustomerId(@PathVariable Long id) {
        log.debug("Find cart by customer id");
        return cartService.findByCustomerId(id);
    }

    @PostMapping("/{id}/{productId}/{count}")
    public void addProductToCart(@PathVariable Long id, @PathVariable Long productId, @PathVariable Integer count) {
        log.debug("Add to cart product");
        cartService.save(id, productId, count);
    }

    @DeleteMapping("/{id}/{productId}")
    public void deleteProductById(@PathVariable Long id, @PathVariable Long productId) {
        log.debug("delete specified product from cart");
        cartService.deleteByCustomerAndProductId(id, productId);
    }

    @DeleteMapping("/{id}")
    public void deleteCartById(@PathVariable Long id) {
        log.debug("delete all from cart");
        cartService.deleteByCustomerId(id);
    }
}
