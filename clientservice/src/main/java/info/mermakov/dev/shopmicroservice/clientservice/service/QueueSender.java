package info.mermakov.dev.shopmicroservice.clientservice.service;

import info.mermakov.dev.shopmicroservice.clientservice.dto.MessageDto;
import info.mermakov.dev.shopmicroservice.clientservice.dto.OrderMessageDto;

public interface QueueSender {
    void send(OrderMessageDto messageDto);

    void send(MessageDto messageDto);
}
