package info.mermakov.dev.shopmicroservice.clientservice.repository;

import info.mermakov.dev.shopmicroservice.clientservice.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Optional<Customer> findByEmail(String email);

    @Query("from Customer where (:firstName is not null and (firstName is not null and firstName like %:firstName%)) " +
            "or (:middleName is not null and (middleName is not null and middleName like %:middleName%)) " +
            "or (:lastName is not null and (lastName is not null and lastName like %:lastName%))")
    List<Customer> findByName(@Param("firstName") String fistName, @Param("middleName") String middleName, @Param("lastName") String lastName);
}
