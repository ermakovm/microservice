package info.mermakov.dev.shopmicroservice.clientservice.service.impl;

import info.mermakov.dev.shopmicroservice.clientservice.config.RabbitConfig;
import info.mermakov.dev.shopmicroservice.clientservice.dto.MessageDto;
import info.mermakov.dev.shopmicroservice.clientservice.dto.OrderMessageDto;
import info.mermakov.dev.shopmicroservice.clientservice.service.QueueSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class QueueSenderImpl implements QueueSender {
    private final RabbitTemplate template;
    private final String orderQueue;
    private final String mailQueue;

    @Autowired
    public QueueSenderImpl(RabbitTemplate template, MessageConverter messageConverter, RabbitConfig config) {
        this.template = template;
        this.template.setMessageConverter(messageConverter);
        this.orderQueue = config.getOrderQueue();
        this.mailQueue = config.getMailQueue();
    }

    @Override
    public void send(OrderMessageDto messageDto) {
        log.debug("send order message");
        template.convertAndSend(orderQueue, messageDto, message -> {
            MessageProperties properties = message.getMessageProperties();
            properties.setDeliveryMode(MessageDeliveryMode.PERSISTENT);
            properties.setContentType(MessageProperties.CONTENT_TYPE_JSON);
            return message;
        });
    }

    @Override
    public void send(MessageDto messageDto) {
        log.debug("send mail message");
        template.convertAndSend(mailQueue, messageDto, message -> {
            MessageProperties properties = message.getMessageProperties();
            properties.setDeliveryMode(MessageDeliveryMode.PERSISTENT);
            properties.setContentType(MessageProperties.CONTENT_TYPE_JSON);
            return message;
        });
    }
}
