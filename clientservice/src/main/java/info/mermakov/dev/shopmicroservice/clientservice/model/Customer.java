package info.mermakov.dev.shopmicroservice.clientservice.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Entity
@Table(name = "customer")
@Data
@NoArgsConstructor
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name", nullable = false, length = 100)
    @Size(min = 1, max = 100)
    private String firstName;

    @Column(name = "middle_name", length = 100)
    @Size(min = 1, max = 100)
    private String middleName;

    @Column(name = "last_name", nullable = false, length = 100)
    @Size(min = 1, max = 100)
    private String lastName;

    @Column(name = "email", unique = true, nullable = false, length = 100)
    @Size(min = 1, max = 100)
    @Email
    private String email;
}
