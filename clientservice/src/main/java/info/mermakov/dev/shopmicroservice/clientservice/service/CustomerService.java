package info.mermakov.dev.shopmicroservice.clientservice.service;

import info.mermakov.dev.shopmicroservice.clientservice.dto.CustomerDto;
import info.mermakov.dev.shopmicroservice.clientservice.dto.OrderDto;
import info.mermakov.dev.shopmicroservice.clientservice.model.Customer;

import java.util.List;

public interface CustomerService {
    List<CustomerDto> findAll();

    List<CustomerDto> findByName(String firstName, String middleName, String lastName);

    CustomerDto findById(Long id);

    CustomerDto findByEmail(String email);

    Customer getById(Long id);

    CustomerDto save(CustomerDto customerDto);

    void removeById(Long id);

    List<OrderDto> findOrdersByCustomerId(Long id);

    CustomerDto convertToDto(Customer customer);

    List<CustomerDto> convertToDto(Iterable<Customer> customers);
}
