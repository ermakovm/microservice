create user client with password 'client';
create database client;
grant all privileges on database client to client;

create user store with password 'store';
create database store;
grant all privileges on database store to store;

create user catalog with password 'catalog';
create database catalog;
grant all privileges on database catalog to catalog;