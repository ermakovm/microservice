package info.mermakov.dev.shopmicroservice.mailservice.service;

import com.rabbitmq.client.Channel;
import info.mermakov.dev.shopmicroservice.mailservice.dto.MessageDto;

import java.io.IOException;

public interface QueueListener {
    void receive(MessageDto messageDto, Channel channel, long tag) throws IOException;
}
