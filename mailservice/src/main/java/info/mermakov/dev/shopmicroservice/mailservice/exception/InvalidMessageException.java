package info.mermakov.dev.shopmicroservice.mailservice.exception;

public class InvalidMessageException extends RuntimeException {
    private static final String MESSAGE = "Invalid message from Rabbit Queue";

    public InvalidMessageException() {
        super(MESSAGE);
    }
}
