package info.mermakov.dev.shopmicroservice.mailservice.service.impl;

import info.mermakov.dev.shopmicroservice.mailservice.dto.MessageDto;
import info.mermakov.dev.shopmicroservice.mailservice.service.MailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.validation.Valid;

@Service
@Slf4j
public class MailServiceImpl implements MailService {
    private final JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    private String messageFrom;

    @Autowired
    public MailServiceImpl(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public void send(@Valid MessageDto messageDto) {
        log.debug("Create mail message");
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(messageFrom);
        mailMessage.setTo(messageDto.getEmail());
        mailMessage.setSubject(messageDto.getSubject());
        mailMessage.setText(messageDto.getText());
        log.debug("Send email");
        mailSender.send(mailMessage);
    }
}
