package info.mermakov.dev.shopmicroservice.mailservice.service;

import info.mermakov.dev.shopmicroservice.mailservice.dto.MessageDto;

public interface MailService {
    void send(MessageDto messageDto);
}
