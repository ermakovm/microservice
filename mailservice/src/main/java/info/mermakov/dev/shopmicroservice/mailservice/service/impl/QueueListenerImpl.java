package info.mermakov.dev.shopmicroservice.mailservice.service.impl;

import com.rabbitmq.client.Channel;
import info.mermakov.dev.shopmicroservice.mailservice.dto.MessageDto;
import info.mermakov.dev.shopmicroservice.mailservice.service.MailService;
import info.mermakov.dev.shopmicroservice.mailservice.service.QueueListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Slf4j
public class QueueListenerImpl implements QueueListener {
    private final MailService mailService;
    private final Queue queue;

    @Autowired
    public QueueListenerImpl(MailService mailService, Queue queue) {
        this.mailService = mailService;
        this.queue = queue;
    }

    @RabbitListener(queues = "${project.queue.mail-queue}", ackMode = "MANUAL")
    @Override
    public void receive(MessageDto messageDto, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws IOException {
        log.debug("Get message from queue: " + queue.getName());
        try {
            mailService.send(messageDto);
        } catch (RuntimeException e) {
            log.error("Sending message failed", e);
        }
        log.debug("Set tag");
        channel.basicAck(tag, false);
    }
}
