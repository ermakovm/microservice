package info.mermakov.dev.shopmicroservice.mailservice.service;

import info.mermakov.dev.shopmicroservice.mailservice.dto.MessageDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application.properties")
public class MailServiceTest {
    @Autowired
    private MailService service;

    @MockBean
    private JavaMailSender sender;

    @MockBean
    private QueueListener listener;

    @Test
    public void sendTest() {
        MessageDto valid = new MessageDto();
        valid.setEmail("test@test.test");
        valid.setSubject("Subject");
        valid.setText("Some Text");
        service.send(valid);
    }
}
