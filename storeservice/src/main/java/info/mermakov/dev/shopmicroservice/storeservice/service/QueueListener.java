package info.mermakov.dev.shopmicroservice.storeservice.service;

import com.rabbitmq.client.Channel;
import info.mermakov.dev.shopmicroservice.storeservice.dto.OrderMessageDto;

import java.io.IOException;

public interface QueueListener {
    void receive(OrderMessageDto messageDto, Channel channel, long tag) throws IOException;
}
