package info.mermakov.dev.shopmicroservice.storeservice.service;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

@org.springframework.cloud.openfeign.FeignClient("CATALOG-SERVICE")
public interface FeignClient {
    @PutMapping("/api/catalog/products/{id}/hide")
    void hideProduct(@PathVariable Long id);

    @PutMapping("/api/catalog/products/{id}/show")
    void showProduct(@PathVariable Long id);
}
