package info.mermakov.dev.shopmicroservice.storeservice.service.impl;

import info.mermakov.dev.shopmicroservice.storeservice.dto.OrderMessageDto;
import info.mermakov.dev.shopmicroservice.storeservice.dto.Pair;
import info.mermakov.dev.shopmicroservice.storeservice.dto.ResultMessageDto;
import info.mermakov.dev.shopmicroservice.storeservice.dto.StockDto;
import info.mermakov.dev.shopmicroservice.storeservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.storeservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.storeservice.model.Stock;
import info.mermakov.dev.shopmicroservice.storeservice.repository.StockRepository;
import info.mermakov.dev.shopmicroservice.storeservice.service.FeignClient;
import info.mermakov.dev.shopmicroservice.storeservice.service.QueueSender;
import info.mermakov.dev.shopmicroservice.storeservice.service.StockService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static info.mermakov.dev.shopmicroservice.storeservice.exception.ErrorMessage.ALREADY_EXIST_ERROR;
import static info.mermakov.dev.shopmicroservice.storeservice.exception.ErrorMessage.NO_FOUND_BY_ID_ERROR;
import static info.mermakov.dev.shopmicroservice.storeservice.exception.ErrorMessage.NO_FOUND_ERROR;

@Slf4j
@Service
public class StockServiceImpl implements StockService {
    private final StockRepository repository;
    private final QueueSender queueSender;
    private final FeignClient feignClient;

    @Autowired
    public StockServiceImpl(StockRepository repository, QueueSender queueSender, FeignClient feignClient) {
        this.repository = repository;
        this.queueSender = queueSender;
        this.feignClient = feignClient;
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public List<StockDto> findAll() {
        log.debug("get data from db");
        List<Stock> stocks = repository.findAll();
        if (stocks.size() > 0) {
            return convertToDto(stocks);
        }
        throw new ResourceNotFoundException(NO_FOUND_ERROR);
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public List<StockDto> findOver() {
        log.debug("get data from db");
        List<Stock> stocks = repository.findOver();
        if (stocks.size() > 0) {
            return convertToDto(stocks);
        }
        throw new ResourceNotFoundException(NO_FOUND_ERROR);
    }

    private Stock getStockById(Long id) {
        log.debug("get from db by id");
        Optional<Stock> stock = repository.findById(id);
        return stock.orElseThrow(() -> new ResourceNotFoundException(NO_FOUND_BY_ID_ERROR + id));
    }

    private Stock getStockByProductId(Long id) {
        log.debug("get from db by productId");
        Optional<Stock> stock = repository.findByProductId(id);
        return stock.orElseThrow(() -> new ResourceNotFoundException(NO_FOUND_BY_ID_ERROR + id));
    }

    private Stock getStockOrNull(Long id) {
        log.debug("get from db by productId");
        Optional<Stock> stock = repository.findByProductId(id);
        return stock.orElse(null);
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public StockDto findById(Long id) {
        return convertToDto(getStockById(id));
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public StockDto findByProductId(Long id) {
        return convertToDto(getStockByProductId(id));
    }

    private boolean reserve(Long id, Integer count) {
        log.debug("reserve by productId");
        Stock stock = getStockOrNull(id);
        if (stock == null) {
            return false;
        }
        Integer av = stock.getAvailable();
        if (av - count < 0) {
            return false;
        }
        Integer newAw = av - count;
        stock.setAvailable(newAw);
        repository.saveAndFlush(stock);
        if (newAw == 0) {
            log.debug("hide product");
            feignClient.hideProduct(id);
        } else if (av == 0) {
            log.debug("Show product on return");
            feignClient.showProduct(id);
        }
        return true;
    }

    private void rollBackItems(List<Pair> products) {
        log.debug("rollback");
        for (Pair pair : products) {
            Stock stock = getStockByProductId(pair.getProductId());
            Integer av = stock.getAvailable();
            Integer newAv = av + pair.getCount();
            stock.setAvailable(newAv);
            repository.saveAndFlush(stock);
            if (av == 0) {
                feignClient.showProduct(pair.getProductId());
            }
        }
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public void reserve(@Validated OrderMessageDto orderDto) {
        log.debug("reserve");
        Long id = orderDto.getOrderId();
        List<Pair> products = orderDto.getProducts();
        List<Pair> toRollback = new ArrayList<>();
        boolean correct = true;
        for (Pair pair : products) {
            if (!correct) {
                rollBackItems(toRollback);
                return;
            }
            correct = reserve(pair.getProductId(), pair.getCount());
            toRollback.add(pair);
        }
        if (!orderDto.getReturnProduct()) {
            ResultMessageDto resultMessageDto = new ResultMessageDto(id, correct);
            log.debug("send message");
            queueSender.sendResult(resultMessageDto);
        }
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public StockDto save(@Validated StockDto stockDto) {
        Stock stock;
        if (stockDto.getId() != null) {
            stock = getStockById(stockDto.getId());
        } else {
            stock = new Stock();
        }
        Stock testStock = getStockOrNull(stockDto.getProductId());
        if (testStock != null && !testStock.getId().equals(stockDto.getId())) {
            throw new InvalidRequestException(ALREADY_EXIST_ERROR);
        }
        BeanUtils.copyProperties(stockDto, stock, "id");
        log.debug("save stock  ");
        Stock result = repository.saveAndFlush(stock);
        if (result.getAvailable() > 0) {
            log.debug("Show product on update");
            feignClient.showProduct(result.getId());
        }
        return convertToDto(result);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public void removeById(Long id) {
        Stock stock = getStockById(id);
        log.debug("remove from db");
        repository.delete(stock);
    }

    @Override
    public StockDto convertToDto(Stock stock) {
        StockDto result = new StockDto();
        BeanUtils.copyProperties(stock, result);
        return result;
    }

    @Override
    public List<StockDto> convertToDto(Iterable<Stock> stocks) {
        List<StockDto> result = new ArrayList<>();
        for (Stock stock : stocks) {
            result.add(convertToDto(stock));
        }
        return result;
    }
}
