package info.mermakov.dev.shopmicroservice.storeservice.service.impl;

import info.mermakov.dev.shopmicroservice.storeservice.config.RabbitConfig;
import info.mermakov.dev.shopmicroservice.storeservice.dto.ResultMessageDto;
import info.mermakov.dev.shopmicroservice.storeservice.service.QueueSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class QueueSenderImpl implements QueueSender {
    private final RabbitTemplate template;

    private String queueName;

    @Autowired
    public QueueSenderImpl(RabbitTemplate template, MessageConverter messageConverter, RabbitConfig config) {
        this.template = template;
        this.template.setMessageConverter(messageConverter);
        this.queueName = config.getStatusQueue();
    }

    @Override
    public void sendResult(ResultMessageDto messageDto) {
        log.debug("Send result to client service");
        template.convertAndSend(queueName, messageDto, message -> {
            MessageProperties properties = message.getMessageProperties();
            properties.setContentType(MessageProperties.CONTENT_TYPE_JSON);
            properties.setDeliveryMode(MessageDeliveryMode.PERSISTENT);
            return message;
        });
    }
}
