package info.mermakov.dev.shopmicroservice.storeservice.exception;

public class ErrorMessage {
    public static final String NO_FOUND_ERROR = "Not found any elements";
    public static final String NO_FOUND_BY_ID_ERROR = "Not found element(s) by id ";
    public static final String ALREADY_EXIST_ERROR = "Element with specified product_id already exist";
}
