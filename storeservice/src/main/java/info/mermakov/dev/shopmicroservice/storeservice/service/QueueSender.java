package info.mermakov.dev.shopmicroservice.storeservice.service;

import info.mermakov.dev.shopmicroservice.storeservice.dto.ResultMessageDto;

public interface QueueSender {
    void sendResult(ResultMessageDto messageDto);
}
