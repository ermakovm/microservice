package info.mermakov.dev.shopmicroservice.storeservice.service;

import info.mermakov.dev.shopmicroservice.storeservice.dto.OrderMessageDto;
import info.mermakov.dev.shopmicroservice.storeservice.dto.StockDto;
import info.mermakov.dev.shopmicroservice.storeservice.model.Stock;

import java.util.List;

public interface StockService {
    List<StockDto> findAll();

    List<StockDto> findOver();

    StockDto findById(Long id);

    StockDto findByProductId(Long id);

    void reserve(OrderMessageDto orderDto);

    StockDto save(StockDto stockDto);

    void removeById(Long id);

    StockDto convertToDto(Stock stock);

    List<StockDto> convertToDto(Iterable<Stock> stocks);
}
