package info.mermakov.dev.shopmicroservice.storeservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StockDto {
    @Positive
    private Long id;

    @Positive
    private Long productId;

    @PositiveOrZero
    private Integer available;
}
