package info.mermakov.dev.shopmicroservice.storeservice.repository;

import info.mermakov.dev.shopmicroservice.storeservice.model.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface StockRepository extends JpaRepository<Stock, Long> {
    @Query("from Stock where available = 0")
    List<Stock> findOver();

    Optional<Stock> findByProductId(Long id);
}
