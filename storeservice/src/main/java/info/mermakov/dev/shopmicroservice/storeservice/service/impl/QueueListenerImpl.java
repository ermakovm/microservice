package info.mermakov.dev.shopmicroservice.storeservice.service.impl;

import com.rabbitmq.client.Channel;
import info.mermakov.dev.shopmicroservice.storeservice.dto.OrderMessageDto;
import info.mermakov.dev.shopmicroservice.storeservice.service.QueueListener;
import info.mermakov.dev.shopmicroservice.storeservice.service.StockService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Slf4j
@Service
public class QueueListenerImpl implements QueueListener {
    private final StockService stockService;

    @Autowired
    public QueueListenerImpl(StockService stockService) {
        this.stockService = stockService;
    }

    @RabbitListener(queues = "${project.queue.order-queue}", ackMode = "MANUAL")
    @Override
    public void receive(OrderMessageDto messageDto, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws IOException {
        log.debug("Message received");
        stockService.reserve(messageDto);
        log.debug("Set ack");
        channel.basicAck(tag, false);
    }
}
