package info.mermakov.dev.shopmicroservice.storeservice.controller;

import info.mermakov.dev.shopmicroservice.storeservice.dto.StockDto;
import info.mermakov.dev.shopmicroservice.storeservice.service.StockService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/api/store/stocks")
public class StockController {
    private final StockService stockService;

    @Autowired
    public StockController(StockService stockService) {
        this.stockService = stockService;
    }

    @GetMapping
    public List<StockDto> findAll() {
        log.debug("Find all stocks");
        return stockService.findAll();
    }

    @GetMapping("/over")
    public List<StockDto> findOver() {
        log.debug("find over stocks");
        return stockService.findOver();
    }

    @GetMapping("/{id}")
    public StockDto findById(@PathVariable Long id) {
        log.debug("Get stock by id");
        return stockService.findById(id);
    }

    @GetMapping("/product/{id}")
    public StockDto findByProductId(@PathVariable Long id) {
        log.debug("Get stock by product id");
        return stockService.findByProductId(id);
    }

    @PostMapping
    public StockDto save(@Validated @RequestBody StockDto stockDto) {
        log.debug("Create new stock");
        stockDto.setId(null);
        return stockService.save(stockDto);
    }

    @PutMapping("/{id}")
    public StockDto save(@Validated @RequestBody StockDto stockDto, @PathVariable Long id) {
        log.debug("Update stock");
        stockDto.setId(id);
        return stockService.save(stockDto);
    }

    @DeleteMapping("/{id}")
    public void removeById(@PathVariable Long id) {
        log.debug("remove stock");
        stockService.removeById(id);
    }
}
