package info.mermakov.dev.shopmicroservice.storeservice.service;

import info.mermakov.dev.shopmicroservice.storeservice.dto.OrderMessageDto;
import info.mermakov.dev.shopmicroservice.storeservice.dto.Pair;
import info.mermakov.dev.shopmicroservice.storeservice.dto.StockDto;
import info.mermakov.dev.shopmicroservice.storeservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.storeservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.storeservice.model.Stock;
import info.mermakov.dev.shopmicroservice.storeservice.repository.StockRepository;
import info.mermakov.dev.shopmicroservice.storeservice.service.util.StockServiceTestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static info.mermakov.dev.shopmicroservice.storeservice.service.util.StockServiceTestUtil.STOCK_1;
import static info.mermakov.dev.shopmicroservice.storeservice.service.util.StockServiceTestUtil.STOCK_2;
import static info.mermakov.dev.shopmicroservice.storeservice.service.util.StockServiceTestUtil.STOCK_DTO_1;
import static info.mermakov.dev.shopmicroservice.storeservice.service.util.StockServiceTestUtil.STOCK_DTO_2;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application.properties")
public class StockServiceTest {
    @Autowired
    private StockService service;

    @MockBean
    private StockRepository repository;

    @MockBean
    private QueueSender sender;

    @MockBean
    private QueueListener listener;

    @MockBean
    private FeignClient client;

    private List<Stock> stocks;

    @BeforeEach
    public void init() {
        stocks = new ArrayList<>();
        stocks.add(STOCK_1);
        stocks.add(STOCK_2);
        StockServiceTestUtil.initData();
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(STOCK_1));
        Mockito.when(repository.findByProductId(1L)).thenReturn(Optional.of(STOCK_1));
    }

    @Test
    public void findAllTest() {
        Mockito.when(repository.findAll()).thenReturn(stocks);
        List<StockDto> result = service.findAll();
        assertEquals(2, result.size());
        assertTrue(result.contains(STOCK_DTO_1));
        assertTrue(result.contains(STOCK_DTO_2));
    }

    @Test
    public void findOverTest() {
        List<Stock> data = new ArrayList<>();
        data.add(STOCK_2);
        Mockito.when(repository.findOver()).thenReturn(data);
        List<StockDto> result = service.findOver();
        assertEquals(1, result.size());
        assertTrue(result.contains(STOCK_DTO_2));
    }

    @Test
    public void findExceptionTest() {
        Mockito.when(repository.findAll()).thenReturn(new ArrayList<>());
        Mockito.when(repository.findOver()).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> service.findAll());
        assertThrows(ResourceNotFoundException.class, () -> service.findOver());
    }

    @Test
    public void findByIdTest() {
        assertEquals(STOCK_DTO_1, service.findById(1L));
        assertEquals(STOCK_DTO_1, service.findByProductId(1L));
    }

    @Test
    public void findByIdExceptionTest() {
        Mockito.when(repository.findById(3L)).thenReturn(Optional.empty());
        Mockito.when(repository.findByProductId(3L)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> service.findById(3L));
        assertThrows(ResourceNotFoundException.class, () -> service.findByProductId(3L));
    }

    @Test
    public void deleteTest() {
        service.removeById(1L);
    }

    @Test
    public void saveTest() {
        Stock stock = new Stock(null, 2L, 0);
        Mockito.when(repository.findByProductId(2L)).thenReturn(Optional.empty());
        Mockito.when(repository.saveAndFlush(stock)).thenReturn(STOCK_2);
        StockDto stockDto = new StockDto(null, 2L, 0);
        assertEquals(STOCK_DTO_2, service.save(stockDto));
    }

    @Test
    public void updateTest() {
        Stock stock = new Stock(2L, 2L, 30);
        Mockito.when(repository.findByProductId(2L)).thenReturn(Optional.of(STOCK_2));
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(STOCK_2));
        Mockito.when(repository.saveAndFlush(stock)).thenReturn(stock);
        StockDto stockDto = new StockDto(2L, 2L, 30);
        assertEquals(stockDto, service.save(stockDto));
    }

    @Test
    public void saveExceptionTest() {
        Stock stock = new Stock(null, 2L, 0);
        Mockito.when(repository.findByProductId(2L)).thenReturn(Optional.of(STOCK_2));
        StockDto stockDto = new StockDto(null, 2L, 0);
        assertThrows(InvalidRequestException.class, () -> service.save(stockDto));
    }

    @Test
    public void reserveTest() {
        OrderMessageDto messageDto = new OrderMessageDto();
        messageDto.setOrderId(1L);
        messageDto.setReturnProduct(false);
        List<Pair> products = new ArrayList<>();
        products.add(new Pair(1L, 5));
        products.add(new Pair(2L, 3));
        products.add(new Pair(3L, 100));
        messageDto.setProducts(products);

        Mockito.when(repository.findByProductId(2L)).thenReturn(Optional.of(STOCK_2));
        Mockito.when(repository.findByProductId(3L)).thenReturn(Optional.empty());
        service.reserve(messageDto);
    }
}
