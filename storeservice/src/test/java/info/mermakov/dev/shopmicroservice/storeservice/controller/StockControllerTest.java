package info.mermakov.dev.shopmicroservice.storeservice.controller;

import info.mermakov.dev.shopmicroservice.storeservice.dto.StockDto;
import info.mermakov.dev.shopmicroservice.storeservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.storeservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.storeservice.service.StockService;
import info.mermakov.dev.shopmicroservice.storeservice.service.util.StockServiceTestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static info.mermakov.dev.shopmicroservice.storeservice.service.util.StockServiceTestUtil.STOCK_DTO_1;
import static info.mermakov.dev.shopmicroservice.storeservice.service.util.StockServiceTestUtil.STOCK_DTO_2;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(StockController.class)
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application.properties")
public class StockControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private StockService service;

    @BeforeEach
    public void init() {
        List<StockDto> stockDtos = new ArrayList<>();
        stockDtos.add(STOCK_DTO_1);
        stockDtos.add(STOCK_DTO_2);
        Mockito.when(service.findAll()).thenReturn(stockDtos);
        Mockito.when(service.findOver()).thenReturn(stockDtos);
        StockServiceTestUtil.initData();
    }

    @Test
    public void findAllTest() throws Exception {
        mvc.perform(get("/api/store/stocks"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].productId").isNotEmpty());
    }

    @Test
    public void findOverTest() throws Exception {
        mvc.perform(get("/api/store/stocks/over"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].productId").isNotEmpty());
    }

    @Test
    public void findByIdTest() throws Exception {
        Mockito.when(service.findById(1L)).thenReturn(STOCK_DTO_1);
        mvc.perform(get("/api/store/stocks/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId").isNotEmpty());
    }

    @Test
    public void findByProductIdTest() throws Exception {
        Mockito.when(service.findByProductId(1L)).thenReturn(STOCK_DTO_1);
        mvc.perform(get("/api/store/stocks/product/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId").isNotEmpty());
    }

    @Test
    public void saveTest() throws Exception {
        Mockito.when(service.save(any())).thenReturn(STOCK_DTO_1);
        mvc.perform(post("/api/store/stocks")
                .content("{\"productId\":1,\"available\":15}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        mvc.perform(put("/api/store/stocks/1")
                .content("{\"productId\":1,\"available\":15}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void deleteTest() throws Exception {
        mvc.perform(delete("/api/store/stocks/1"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void exceptionTest() throws Exception {
        mvc.perform(post("/api/store/stocks")
                .content("{\"productId\":-1,\"available\":-10}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());

        Mockito.when(service.findAll()).thenThrow(ResourceNotFoundException.class);
        mvc.perform(get("/api/store/stocks"))
                .andDo(print())
                .andExpect(status().isNotFound());

        Mockito.when(service.save(any())).thenThrow(InvalidRequestException.class);
        mvc.perform(post("/api/store/stocks")
                .content("{\"productId\":1,\"available\":15}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
}
