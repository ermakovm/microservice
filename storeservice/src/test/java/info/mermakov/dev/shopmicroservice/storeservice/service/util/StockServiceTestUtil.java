package info.mermakov.dev.shopmicroservice.storeservice.service.util;

import info.mermakov.dev.shopmicroservice.storeservice.dto.StockDto;
import info.mermakov.dev.shopmicroservice.storeservice.model.Stock;

public class StockServiceTestUtil {
    public static final StockDto STOCK_DTO_1;
    public static final StockDto STOCK_DTO_2;
    public static final Stock STOCK_1;
    public static final Stock STOCK_2;

    static {
        STOCK_DTO_1 = new StockDto();
        STOCK_DTO_2 = new StockDto();
        STOCK_1 = new Stock();
        STOCK_2 = new Stock();
        initData();
    }

    public static void initData() {
        STOCK_1.setId(1L);
        STOCK_1.setProductId(1L);
        STOCK_1.setAvailable(15);

        STOCK_2.setId(2L);
        STOCK_2.setProductId(2L);
        STOCK_2.setAvailable(0);

        STOCK_DTO_1.setId(1L);
        STOCK_DTO_1.setProductId(1L);
        STOCK_DTO_1.setAvailable(15);

        STOCK_DTO_2.setId(2L);
        STOCK_DTO_2.setProductId(2L);
        STOCK_DTO_2.setAvailable(0);
    }
}
