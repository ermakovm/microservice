# Microservice test project

## Dependencies
1) spring-cloud-starter-netflix-eureka-server
2) spring-boot-starter-actuator
3) caffeine
4) spring-cloud-config-server
5) spring-cloud-starter-netflix-eureka-client
6) spring-cloud-config-client
7) spring-cloud-starter-gateway
8) spring-boot-starter-web
9) spring-boot-starter-data-jpa
10) flyway-core
11) postgresql
12) spring-boot-starter-cache
13) hibernate-jcache
14) ehcache
15) hibernate-types-52
16) spring-cloud-starter-openfeign
17) spring-boot-starter-amqp
18) spring-boot-starter-mail
19) spring-boot-starter-test
20) junit-jupiter

## Config repository
[Config repository](https://github.com/ermakovm/microservice-config.git)

## Running
1) Set `JAVA_OPTS=-DGIT_CONFIG` for link to git repository with config
2) Update `mail-service.properties` to you mail server

## Usage
1) Store service
    * Queries:
        1) Method = GET, URL = `/api/store/stocks`. Returns a pairs of product index and available quantity.
        2) Method = GET, URL = `/api/store/stocks/over`. Returns finished goods.
        3) Method = GET, URL = `/api/store/stocks/{id}`. Returns a pairs of product index and available quantity by specified index.
        4) Method = GET, URL = `/api/store/stocks/product/{id}`. Return available product by `id`.
        5) Method = POST, URL = `/api/store/stocks`, Body = 
            ```
            {
                "productId": {Long},
                "available": {Integer}
            }
            ```
            Add new product to stock.
        6) Method = PUT, URL = `/api/store/stocks/{id}`, Body = 
            ```
            {
                "productId": {Long},
                "available": {Integer}
            }
            ```
            Update available products.
        7) Method = DELETE, URL = `/api/store/stocks/{id}`. Delete information by id.
       
2) Client service
    * Queries:
        1) Method = GET, URL = `/api/client/customers`, Body = 
            ```
            {
                "firstName":"name_to_search",
                "middleName":"name_to_search",
                "lastName":"name_to_search"
            }
            ```
            When body empty return all customers, otherwise return with the specified name.
    
        2)  Method = GET, URL = `/api/client/customers/email`, Body = `{"email":"email_to_search"}`. Find customers with specified email.
    
        3) Method = GET, URL = `/api/client/customers/{id}`. Return customer by `id`.
    
        4) Method = GET, URL = `/api/client/customers/{id}/orders`. Return orders by customers with `id`.
   
        5) Method = POST, URL = `/api/client/customers`, Body = 
            ```
            {
                "firstName":"name_to_add",
                "middleName":"name_to_add",
                "lastName":"name_to_add",
                "email":"email_to_add"
            }
            ```
            Add new client.

        6) Method = PUT, URL = `/api/client/customers/{id}`, Body = 
            ```
            {
                "firstName":"name_to_update",
                "middleName":"name_to_update",
                "lastName":"name_to_update",
                "email":"email_to_update"
            } 
            ```
            Update customer with `id`.
               
        7) Method = DELETE, URL = `/api/client/customers/{id}`. Delete customer with `id`.

        8) Method = GET, URL = `/api/client/carts/{id}` Return cart of customer with `id`.
    
        9) Method = POST, URL = `/api/client/carts/{id}/{productId}/{count}` Add to cart (customer with `id`), product with `productId` and `count` of products.
    
        10) Method = DELETE, URL = `/api/client/carts/{id}/{productId}/`. Remove product with `productId` from customer cart (customer with `id`).
    
        11) Method = DELETE, URL = `/api/client/carts/{id}`. Delete all product from customer cart with customer `id`.
    
        12) Method = GET, URL = `/api/client/orders/{id}`. Get order by order `id`.
    
        13) Method = POST, URL = `/api/client/orders/{id}`. Create order using cart of customer with `id`.
    
        14) Method = PATCH, URL = `/api/client/orders/{id}`. Set completion date of order with `id`.
    
        15) Method = DELETE, URL = `/api/client/orders/{id}`. Delete order with `id`.
    
3) Catalog service
    * Queries:
        1) Method = GET, URL = `/api/catalog/categories`, Body = 
            ```
            {
                "name":"name_to_search"
            }
            ```
            When body empty return all categories, otherwise return with the specified name.
                 
        2) Method = GET, URL = `/api/catalog/categories/{id}`. Return category by `id`.
        
        3) Method = GET, URL = `/api/catalog/categories/{id}/products`. Return products of category with `id`.
        
        4) Method = POST, URL = `/api/catalog/categories`, Body =
            ```
            {
                "name":"name_to_add"
            }
            ``` 
            Add new category.
        
        5) Method = PUT, URL = `/api/catalog/categories/{id}`, Body = 
            ```
            {
                "name":"name_to_update"
            }
            ```
            Update category with `id`.
        
        6) Method = DELETE, URL = `/api/catalog/categories{id}`. Delete category with `id`.
               
        7) Method = GET, URL = `/api/catalog/manufacturers`, Body = 
            ```
            {
                "name":"name_for_search"
            }
            ```
            When body empty return all manufacturers, otherwise return with the specified name. It return only visible manufacturers.
                   
        8) Method = GET, URL = `/api/catalog/manufacturers/all`. Return all manufacturers (with hide).
               
        9) Method = GET, URL = `/api/catalog/manufacturers/{id}`. Return manufacturer by `id`.
       
        10) Method = GET, URL = `/api/catalog/manufacturers/{id}/products`. Return products of manufacturer with `id`.
                
        11) Method = POST, URL = `/api/catalog/manufacturers`, Body = 
            ```
            {
                "name":"name_to_add"
            }
            ```
            Add new manufacturer.
                   
        12) Method = PUT, URL = `/api/catalog/manufacturers/{id}`, Body = 
            ```
            {
                "name":"name_to_update"
            }           
            ```
            Update manufacturer with `id`.
        13) Method = DELETE, URL = `/api/catalog/manufacturers/{id}` Delete manufacturer with `id`.
        
        14) Method = PUT, URL = `/api/catalog/manufacturers/{id}/hide`. Hide manufacturer with `id` and its products.
        
        15) Method = PUT, URL = `/api/catalog/manufacturers/{id}/show`. Show manufacturer with `id`. 
        
        16) Method = GET, URL = `/api/catalog/products`, Body = 
            ```
            {
                "name":"name_for_search"
            }
            ```
            When body empty return all products, otherwise return with the specified name. It return only visible products.
        
        17) Method = GET, URL = `/api/catalog/products/all`. Return all products (with hide).
        
        18) Method = GET, URL = `/api/catalog/products/{id}`. Return product by `id`.
        
        19) Method = POST, URL = `/api/catalog/products`, Body =
            ```
            {
                "name": "name_to_add",
                "cost: {Double},
                "manufacturer":
                    {
                        "name": "manufacturer_name"
                    },
                "metaData":
                    {
                        "Key": "Value",
                        "Key2": "Value2"
                    },
                "categories":
                [
                    {
                        "name": "category_name"
                    },
                    {
                        "name": "category2_name"
                    }
                ]
            }
            ```
            Add new product
        
        20) Method = PUT, URL = `/api/catalog/products/{id}`, Body = 
            ```
            {
                "name": "name_to_add",
                "cost: {Double},
                "manufacturer":
                    {
                        "name": "manufacturer_name"
                    },
                "metaData":
                    {
                        "Key": "Value",
                        "Key2": "Value2"
                    },
                "categories":
                    [
                        {
                            "name": "category_name"
                        },
                        {
                            "name": "category2_name"
                        }
                    ]
            }    
            ```
            Update product with `id`
                
        21) Method = DELETE, URL = `/api/catalog/products/{id}`. Remove product with `id`.
        
        22) Method  = PUT, URL = `/api/catalog/products/{id}/hide`. Hide product with `id`.
        
        23) Method = PUT, URL = `/api/catalog/products/{id}/show`. Show product with `id`.
        
        24) Method = GET, URL = `/api/catalog/products/{id}/valid`. Return `true` if product exist and not hide.
        
        25) Method = GET, URL = `/api/catalog/products/{id}/price` Return cost of product with `id` 
        
        26) Method = POST, URL = `/api/catalog/files/product/{id}`, Request params: MultipartFile and `desc` as description of file. Upload file to product with `id`.
        
        27) Method = GET, URL = `/api/catalog/files/download/{filename}`. Resource for download file.
        
        28) Method = DELETE, URL =  `/api/catalog/files/{id}`. Remove file by `id`.
        
        29) Method = GET, URL = `/api/catalog/files/{id}`. Get file info by file `id`.
        
        30) Method = GET, URL = `/api/catalog/files`. Get info of all files.
        
        31) Method = PUT, URL = `/api/catalog/files/{id}`, Param `desc` as description. Update description of file with `id`.
