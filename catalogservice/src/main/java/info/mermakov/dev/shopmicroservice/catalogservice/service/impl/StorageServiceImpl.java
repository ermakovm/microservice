package info.mermakov.dev.shopmicroservice.catalogservice.service.impl;

import info.mermakov.dev.shopmicroservice.catalogservice.config.FileStorageConfig;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.FileStorageException;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.catalogservice.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.UUID;

import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.FILE_NOT_FOUND;
import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.INVALID_EXTENSION_ERROR;
import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.INVALID_FILE;
import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.STORAGE_ERROR;

@Slf4j
@Service
public class StorageServiceImpl implements StorageService {
    private final Path storageLocation;
    private final List<String> extensions;

    @Autowired
    public StorageServiceImpl(FileStorageConfig storageConfig) {
        this.extensions = storageConfig.getExtensions();
        this.storageLocation = Paths.get(storageConfig.getStorageDir()).toAbsolutePath().normalize();
        try {
            Files.createDirectories(storageLocation);
        } catch (IOException exception) {
            throw new FileStorageException(STORAGE_ERROR, exception);
        }
    }

    @Override
    public String storeFile(MultipartFile file) {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
        if (!extensions.contains(extension)) {
            throw new InvalidRequestException(INVALID_EXTENSION_ERROR + extension);
        }
        String localName = UUID.randomUUID().toString() + "-" + System.currentTimeMillis() + "." + extension;
        try {
            log.debug("Get Path to file");
            Path target = storageLocation.resolve(localName);
            log.debug("copy file fo fs");
            Files.copy(file.getInputStream(), target, StandardCopyOption.REPLACE_EXISTING);
            return localName;
        } catch (IOException exception) {
            throw new FileStorageException(STORAGE_ERROR, exception);
        }
    }

    @Override
    public void removeFile(String name) {
        if (name == null) {
            throw new InvalidRequestException(INVALID_FILE);
        }
        log.debug("get Path by name");
        Path path = storageLocation.resolve(name);
        try {
            log.debug("Get File");
            File file = path.toFile();
            if (file.exists() && file.isFile()) {
                log.debug("Delete file");
                file.delete();
            }
        } catch (Exception exception) {
            throw new FileStorageException(STORAGE_ERROR, exception);
        }
    }

    @Override
    public Resource loadFile(String name) {
        if (name == null) {
            throw new InvalidRequestException(INVALID_FILE);
        }
        try {
            log.debug("get Path");
            Path filePath = storageLocation.resolve(name).normalize();
            log.debug("Get Resource");
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new ResourceNotFoundException(FILE_NOT_FOUND);
            }
        } catch (MalformedURLException exception) {
            throw new ResourceNotFoundException(FILE_NOT_FOUND);
        }
    }
}
