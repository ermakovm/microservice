package info.mermakov.dev.shopmicroservice.catalogservice.repository;

import info.mermakov.dev.shopmicroservice.catalogservice.model.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Long> {
    @Query("from ProductCategory where category.id = :id")
    List<ProductCategory> findByCategoryId(@Param("id") Long id);

    @Query("from ProductCategory where product.id = :id")
    List<ProductCategory> findByProductId(@Param("id") Long id);

    @Modifying
    @Query("delete from ProductCategory where category.id = :id")
    void deleteByCategoryId(@Param("id") Long id);

    @Modifying
    @Query("delete from ProductCategory where product.id = :id")
    void deleteByProductId(@Param("id") Long id);
}
