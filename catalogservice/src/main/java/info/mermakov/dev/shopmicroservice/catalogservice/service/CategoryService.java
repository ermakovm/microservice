package info.mermakov.dev.shopmicroservice.catalogservice.service;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.CategoryDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.ProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.model.Category;

import java.util.List;

public interface CategoryService {
    List<CategoryDto> findAllCategories();

    CategoryDto findCategoryById(Long id);

    Category getCategoryOrNullByName(String name);

    List<CategoryDto> findCategoriesByName(String name);

    List<ProductDto> findProductsByCategoryId(Long id);

    Category saveCategory(CategoryDto categoryDto);

    CategoryDto save(CategoryDto categoryDto);

    void removeCategoryById(Long id);

    CategoryDto convertToDto(Category category);

    List<CategoryDto> convertToDto(Iterable<Category> categories);
}
