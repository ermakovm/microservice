package info.mermakov.dev.shopmicroservice.catalogservice.controller;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.CategoryDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.ProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/api/catalog/categories")
public class CategoryController {
    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping
    public List<CategoryDto> findCategories(@RequestBody(required = false) CategoryDto categoryDto) {
        log.debug("Find Categories");
        if (categoryDto == null) {
            return categoryService.findAllCategories();
        }
        return categoryService.findCategoriesByName(categoryDto.getName());
    }

    @GetMapping("/{id}")
    public CategoryDto findCategoryById(@PathVariable Long id) {
        log.debug("Find Category by id");
        return categoryService.findCategoryById(id);
    }

    @GetMapping("/{id}/products")
    public List<ProductDto> findProductsByCategoryId(@PathVariable Long id) {
        log.debug("Find Products by category id");
        return categoryService.findProductsByCategoryId(id);
    }

    @PostMapping
    public CategoryDto saveCategory(@Validated @RequestBody CategoryDto categoryDto) {
        log.debug("Create new category");
        categoryDto.setId(null);
        return categoryService.save(categoryDto);
    }

    @PutMapping("/{id}")
    public CategoryDto saveCategory(@PathVariable Long id, @Validated @RequestBody CategoryDto categoryDto) {
        log.debug("Update category");
        categoryDto.setId(id);
        return categoryService.save(categoryDto);
    }

    @DeleteMapping("/{id}")
    public void deleteCategory(@PathVariable Long id) {
        log.debug("delete Category");
        categoryService.removeCategoryById(id);
    }
}