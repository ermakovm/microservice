package info.mermakov.dev.shopmicroservice.catalogservice.service;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.FullProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.ProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.UpdateProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.model.Product;

import java.util.List;

public interface ProductService {
    List<ProductDto> findProducts();

    List<ProductDto> findAllProducts();

    Product getProductById(Long id);

    FullProductDto findProductById(Long id);

    List<ProductDto> findProductsByName(String name);

    List<Product> findProductsByManufacturerId(Long id);

    UpdateProductDto save(UpdateProductDto productDto);

    void removeProductById(Long id);

    void hideProductById(Long id);

    void hideProductByManufacturerId(Long id);

    void showProductById(Long id);

    ProductDto convertToDto(Product product);

    List<ProductDto> convertToDto(Iterable<Product> products);

    Boolean checkProduct(Long id);

    Double getPrice(Long id);
}
