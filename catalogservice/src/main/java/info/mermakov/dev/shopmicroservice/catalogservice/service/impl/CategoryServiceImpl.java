package info.mermakov.dev.shopmicroservice.catalogservice.service.impl;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.CategoryDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.ProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.catalogservice.model.Category;
import info.mermakov.dev.shopmicroservice.catalogservice.model.Product;
import info.mermakov.dev.shopmicroservice.catalogservice.model.ProductCategory;
import info.mermakov.dev.shopmicroservice.catalogservice.repository.CategoryRepository;
import info.mermakov.dev.shopmicroservice.catalogservice.repository.ProductCategoryRepository;
import info.mermakov.dev.shopmicroservice.catalogservice.service.CategoryService;
import info.mermakov.dev.shopmicroservice.catalogservice.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.ALREADY_EXIST_ERROR;
import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.FIELD_INVALID_ERROR;
import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.NO_FOUND_BY_ID_ERROR;
import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.NO_FOUND_BY_NAME_ERROR;
import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.NO_FOUND_ERROR;

@Slf4j
@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository repository;
    private final ProductCategoryRepository productCategoryRepository;
    private final ProductService productService;

    @Autowired
    public CategoryServiceImpl(CategoryRepository repository,
                               ProductCategoryRepository productCategoryRepository,
                               @Lazy ProductService productService) {
        this.repository = repository;
        this.productCategoryRepository = productCategoryRepository;
        this.productService = productService;
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public List<CategoryDto> findAllCategories() {
        log.debug("Get categories from db");
        List<Category> categories = repository.findAll();
        if (categories.size() > 0) {
            return convertToDto(categories);
        }
        throw new ResourceNotFoundException(NO_FOUND_ERROR);
    }

    private Category getCategoryById(Long id) {
        log.debug("Get category from db byy id");
        Optional<Category> category = repository.findById(id);
        return category.orElseThrow(() -> new ResourceNotFoundException(NO_FOUND_BY_ID_ERROR + id));
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public CategoryDto findCategoryById(Long id) {
        return convertToDto(getCategoryById(id));
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public Category getCategoryOrNullByName(String name) {
        log.debug("Get category from db by name");
        Optional<Category> category = repository.findByName(name);
        return category.orElse(null);
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public List<CategoryDto> findCategoriesByName(String name) {
        if (name == null) {
            throw new InvalidRequestException(FIELD_INVALID_ERROR + "'name'");
        }
        log.debug("Find categories by name in db");
        List<Category> categories = repository.findByNameContains(name);
        if (categories.size() > 0) {
            return convertToDto(categories);
        }
        throw new ResourceNotFoundException(NO_FOUND_BY_NAME_ERROR + name);
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public List<ProductDto> findProductsByCategoryId(Long id) {
        log.debug("Get data from product_category table");
        List<ProductCategory> productCategories = productCategoryRepository.findByCategoryId(id);
        if (productCategories.size() > 0) {
            List<Product> products = new ArrayList<>();
            for (ProductCategory productCategory : productCategories) {
                products.add(productCategory.getProduct());
            }
            return productService.convertToDto(products);
        }
        throw new ResourceNotFoundException(NO_FOUND_BY_ID_ERROR + id);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public Category saveCategory(@Validated CategoryDto categoryDto) {
        Category category;
        if (categoryDto.getId() != null) {
            log.debug("Get category from db");
            category = getCategoryById(categoryDto.getId());
        } else {
            log.debug("Create empty categroy");
            category = new Category();
        }
        Category categoryByName = getCategoryOrNullByName(categoryDto.getName());
        if (categoryByName != null && !categoryByName.getId().equals(categoryDto.getId())) {
            throw new InvalidRequestException(ALREADY_EXIST_ERROR);
        }
        BeanUtils.copyProperties(categoryDto, category, "id");
        log.debug("save Category to db");
        return repository.saveAndFlush(category);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public CategoryDto save(@Validated CategoryDto categoryDto) {
        Category category = saveCategory(categoryDto);
        return convertToDto(category);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public void removeCategoryById(Long id) {
        Category category = getCategoryById(id);
        log.debug("delete data from product_category");
        productCategoryRepository.deleteByCategoryId(id);
        log.debug("delete category from db");
        repository.delete(category);
    }

    @Override
    public CategoryDto convertToDto(Category category) {
        CategoryDto categoryDto = new CategoryDto();
        BeanUtils.copyProperties(category, categoryDto);
        return categoryDto;
    }

    @Override
    public List<CategoryDto> convertToDto(Iterable<Category> categories) {
        List<CategoryDto> result = new ArrayList<>();
        for (Category category : categories) {
            result.add(convertToDto(category));
        }
        return result;
    }
}
