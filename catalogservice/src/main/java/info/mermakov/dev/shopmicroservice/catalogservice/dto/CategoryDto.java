package info.mermakov.dev.shopmicroservice.catalogservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryDto {
    @Positive
    private Long id;

    @NotBlank
    @Size(min = 1, max = 100)
    private String name;
}
