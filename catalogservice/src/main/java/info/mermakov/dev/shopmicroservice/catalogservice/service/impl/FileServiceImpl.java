package info.mermakov.dev.shopmicroservice.catalogservice.service.impl;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.FileDataDto;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.catalogservice.model.FileData;
import info.mermakov.dev.shopmicroservice.catalogservice.model.Product;
import info.mermakov.dev.shopmicroservice.catalogservice.model.ProductFile;
import info.mermakov.dev.shopmicroservice.catalogservice.repository.FileDataRepository;
import info.mermakov.dev.shopmicroservice.catalogservice.repository.ProductFileRepository;
import info.mermakov.dev.shopmicroservice.catalogservice.service.FileService;
import info.mermakov.dev.shopmicroservice.catalogservice.service.ProductService;
import info.mermakov.dev.shopmicroservice.catalogservice.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.FIELD_INVALID_ERROR;
import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.NO_FOUND_BY_ID_ERROR;
import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.NO_FOUND_ERROR;

@Slf4j
@Service
public class FileServiceImpl implements FileService {
    private final StorageService storageService;
    private final FileDataRepository repository;
    private final ProductFileRepository productFileRepository;
    private final ProductService productService;
    private final String PATH = "/api/catalog/files/download";

    @Autowired
    public FileServiceImpl(StorageService storageService,
                           FileDataRepository repository,
                           ProductFileRepository productFileRepository,
                           @Lazy ProductService productService) {
        this.storageService = storageService;
        this.repository = repository;
        this.productFileRepository = productFileRepository;
        this.productService = productService;
    }

    @Override
    public Resource loadFile(String name) {
        log.debug("Get resource from storageService");
        return storageService.loadFile(name);
    }

    private FileData getFileById(Long id) {
        log.debug("Get data from db by id");
        Optional<FileData> fileData = repository.findById(id);
        return fileData.orElseThrow(() -> new ResourceNotFoundException(NO_FOUND_BY_ID_ERROR + id));
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public FileDataDto findById(Long id) {
        return convertToDto(getFileById(id));
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public List<FileDataDto> findFiles() {
        log.debug("Get all data from db");
        List<FileData> files = repository.findAll();
        if (files.size() > 0) {
            return convertToDto(files);
        }
        throw new ResourceNotFoundException(NO_FOUND_ERROR);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public FileDataDto saveFile(MultipartFile file, String description, Long id) {
        log.debug("Get product from productService");
        Product product = productService.getProductById(id);
        log.debug("Store file by storageService");
        String fileName = storageService.storeFile(file);
        String path = PATH + "/" + fileName;

        FileData fileData = new FileData();
        fileData.setName(fileName);
        fileData.setPath(path);
        fileData.setDescription(description);
        log.debug("Save data to db");
        FileData result = repository.saveAndFlush(fileData);

        ProductFile productFile = new ProductFile();
        productFile.setFileData(result);
        productFile.setProduct(product);
        log.debug("Save association in db");
        productFileRepository.saveAndFlush(productFile);

        return convertToDto(result);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public FileDataDto update(String description, Long id) {
        if (description == null || description.length() < 1 || description.length() > 500) {
            throw new InvalidRequestException(FIELD_INVALID_ERROR);
        }
        FileData fileData = getFileById(id);
        fileData.setDescription(description);
        log.debug("Update description");
        return convertToDto(repository.saveAndFlush(fileData));
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public void removeById(Long id) {
        FileData fileData = getFileById(id);
        String fileName = fileData.getName();
        log.debug("remove data from product_file table");
        productFileRepository.deleteByFileId(id);
        log.debug("remove file info from db");
        repository.delete(fileData);
        log.debug("remove file from fs");
        storageService.removeFile(fileName);
    }

    @Override
    public FileDataDto convertToDto(FileData fileData) {
        FileDataDto result = new FileDataDto();
        BeanUtils.copyProperties(fileData, result);
        return result;
    }

    @Override
    public List<FileDataDto> convertToDto(Iterable<FileData> filesData) {
        List<FileDataDto> result = new ArrayList<>();
        for (FileData fileData : filesData) {
            result.add(convertToDto(fileData));
        }
        return result;
    }
}
