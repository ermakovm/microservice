package info.mermakov.dev.shopmicroservice.catalogservice.repository;

import info.mermakov.dev.shopmicroservice.catalogservice.model.Manufacturer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ManufacturerRepository extends JpaRepository<Manufacturer, Long> {
    @org.springframework.data.jpa.repository.QueryHints({
            @javax.persistence.QueryHint(name = "org.hibernate.cacheable", value = "true")
    })
    @Query("from Manufacturer where hide = false")
    List<Manufacturer> findManufacturers();

    @org.springframework.data.jpa.repository.QueryHints({
            @javax.persistence.QueryHint(name = "org.hibernate.cacheable", value = "true")
    })
    @Query("from Manufacturer where name like %:name% and hide = false")
    List<Manufacturer> findByNameContains(@Param("name") String name);

    @org.springframework.data.jpa.repository.QueryHints({
            @javax.persistence.QueryHint(name = "org.hibernate.cacheable", value = "true")
    })
    Optional<Manufacturer> findByName(String name);

    @Modifying
    @Query("update Manufacturer set hide = true where id = :id")
    void hideById(@Param("id") Long id);

    @Modifying
    @Query("update Manufacturer set hide = false where id = :id")
    void showById(@Param("id") Long id);
}
