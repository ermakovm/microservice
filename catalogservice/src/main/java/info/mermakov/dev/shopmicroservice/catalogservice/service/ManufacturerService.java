package info.mermakov.dev.shopmicroservice.catalogservice.service;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.ManufacturerDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.ProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.model.Manufacturer;

import java.util.List;

public interface ManufacturerService {
    List<ManufacturerDto> findManufacturers();

    List<ManufacturerDto> findAllManufacturers();

    ManufacturerDto findManufacturerById(Long id);

    Manufacturer getManufacturerOrNullByName(String name);

    List<ManufacturerDto> findManufacturersByName(String name);

    List<ProductDto> findProductsByManufacturerId(Long id);

    Manufacturer saveManufacturer(ManufacturerDto manufacturerDto);

    ManufacturerDto save(ManufacturerDto manufacturerDto);

    void removeManufacturerById(Long id);

    ManufacturerDto convertToDto(Manufacturer manufacturer);

    List<ManufacturerDto> convertToDto(Iterable<Manufacturer> manufacturers);

    void hideManufacturerById(Long id);

    void showManufacturerById(Long id);
}
