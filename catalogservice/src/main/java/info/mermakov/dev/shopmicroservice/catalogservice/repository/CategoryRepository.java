package info.mermakov.dev.shopmicroservice.catalogservice.repository;

import info.mermakov.dev.shopmicroservice.catalogservice.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    @org.springframework.data.jpa.repository.QueryHints({
            @javax.persistence.QueryHint(name = "org.hibernate.cacheable", value = "true")
    })
    List<Category> findByNameContains(String name);

    @org.springframework.data.jpa.repository.QueryHints({
            @javax.persistence.QueryHint(name = "org.hibernate.cacheable", value = "true")
    })
    Optional<Category> findByName(String name);
}
