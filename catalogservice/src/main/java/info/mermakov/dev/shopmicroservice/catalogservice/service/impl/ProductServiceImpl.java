package info.mermakov.dev.shopmicroservice.catalogservice.service.impl;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.CategoryDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.FullProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.ManufacturerDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.ProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.UpdateProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.catalogservice.model.Category;
import info.mermakov.dev.shopmicroservice.catalogservice.model.FileData;
import info.mermakov.dev.shopmicroservice.catalogservice.model.Manufacturer;
import info.mermakov.dev.shopmicroservice.catalogservice.model.Product;
import info.mermakov.dev.shopmicroservice.catalogservice.model.ProductCategory;
import info.mermakov.dev.shopmicroservice.catalogservice.model.ProductFile;
import info.mermakov.dev.shopmicroservice.catalogservice.repository.ProductCategoryRepository;
import info.mermakov.dev.shopmicroservice.catalogservice.repository.ProductFileRepository;
import info.mermakov.dev.shopmicroservice.catalogservice.repository.ProductRepository;
import info.mermakov.dev.shopmicroservice.catalogservice.service.CategoryService;
import info.mermakov.dev.shopmicroservice.catalogservice.service.FileService;
import info.mermakov.dev.shopmicroservice.catalogservice.service.ManufacturerService;
import info.mermakov.dev.shopmicroservice.catalogservice.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.ALREADY_EXIST_ERROR;
import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.FIELD_INVALID_ERROR;
import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.NO_FOUND_BY_ID_ERROR;
import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.NO_FOUND_BY_NAME_ERROR;
import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.NO_FOUND_ERROR;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService {
    private final ProductRepository repository;
    private final ProductCategoryRepository productCategoryRepository;
    private final ProductFileRepository productFileRepository;
    private final ManufacturerService manufacturerService;
    private final CategoryService categoryService;
    private final FileService fileService;

    @Autowired
    public ProductServiceImpl(ProductRepository repository,
                              ProductCategoryRepository productCategoryRepository,
                              ProductFileRepository productFileRepository,
                              ManufacturerService manufacturerService,
                              CategoryService categoryService,
                              FileService fileService) {
        this.repository = repository;
        this.productCategoryRepository = productCategoryRepository;
        this.productFileRepository = productFileRepository;
        this.manufacturerService = manufacturerService;
        this.categoryService = categoryService;
        this.fileService = fileService;
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public List<ProductDto> findProducts() {
        log.debug("Get data from db");
        List<Product> products = repository.findProducts();
        if (products.size() > 0) {
            return convertToDto(products);
        }
        throw new ResourceNotFoundException(NO_FOUND_ERROR);
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public List<ProductDto> findAllProducts() {
        log.debug("Get data from db");
        List<Product> products = repository.findAll();
        if (products.size() > 0) {
            return convertToDto(products);
        }
        throw new ResourceNotFoundException(NO_FOUND_ERROR);
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public Product getProductById(Long id) {
        log.debug("find by id in db");
        Optional<Product> product = repository.findById(id);
        return product.orElseThrow(() -> new ResourceNotFoundException(NO_FOUND_BY_ID_ERROR + id));
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public FullProductDto findProductById(Long id) {
        Product product = getProductById(id);
        FullProductDto result = new FullProductDto();
        BeanUtils.copyProperties(product, result, "hide", "manufacturer", "categories", "files");
        result.setManufacturer(manufacturerService.convertToDto(product.getManufacturer()));

        List<Category> categories = getCategories(product);
        result.setCategories(categoryService.convertToDto(categories));

        List<FileData> files = getFile(product);
        result.setFiles(fileService.convertToDto(files));

        return result;
    }

    private List<FileData> getFile(Product product) {
        log.debug("Get file data from db");
        List<ProductFile> data = productFileRepository.findByProductId(product.getId());
        List<FileData> result = new ArrayList<>();
        for (ProductFile productFile : data) {
            result.add(productFile.getFileData());
        }
        return result;
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public List<ProductDto> findProductsByName(String name) {
        if (name == null) {
            throw new InvalidRequestException(FIELD_INVALID_ERROR + "'name'");
        }
        List<Product> products = repository.findByNameContains(name);
        if (products.size() > 0) {
            return convertToDto(products);
        }
        throw new ResourceNotFoundException(NO_FOUND_BY_NAME_ERROR + name);
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public List<Product> findProductsByManufacturerId(Long id) {
        return repository.findByManufacturerId(id);
    }

    private List<Category> getCategories(Product product) {
        List<Category> result = new ArrayList<>();
        List<ProductCategory> productCategories = productCategoryRepository.findByProductId(product.getId());
        for (ProductCategory productCategory : productCategories) {
            result.add(productCategory.getCategory());
        }
        return result;
    }

    private void updateCategories(Product product, List<CategoryDto> categoriesDto) {
        Set<Category> newCategories = new HashSet<>();
        for (CategoryDto categoryDto : categoriesDto) {
            Category category = categoryService.getCategoryOrNullByName(categoryDto.getName());
            if (category == null) {
                categoryDto.setId(null);
                category = categoryService.saveCategory(categoryDto);
            }
            newCategories.add(category);
        }
        newCategories.forEach(category -> {
            ProductCategory productCategory = new ProductCategory();
            productCategory.setProduct(product);
            productCategory.setCategory(category);
            log.debug("Update product_category table");
            productCategoryRepository.saveAndFlush(productCategory);
        });
    }

    private Manufacturer getManufacturer(ManufacturerDto manufacturerDto) {
        Manufacturer manufacturer = manufacturerService.getManufacturerOrNullByName(manufacturerDto.getName());
        if (manufacturer == null) {
            manufacturerDto.setId(null);
            return manufacturerService.saveManufacturer(manufacturerDto);
        }
        return manufacturer;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public UpdateProductDto save(@Validated UpdateProductDto productDto) {
        Product product;
        if (productDto.getId() != null) {
            product = getProductById(productDto.getId());
        } else {
            product = new Product();
            product.setHide(false);
        }
        List<Product> products = repository.findByNameContains(productDto.getName());
        for (Product pr : products) {
            final double threshold = .0001;
            if (pr.getName().equals(productDto.getName())
                    && pr.getManufacturer().getName().equals(productDto.getManufacturer().getName())
                    && Math.abs(pr.getCost() - productDto.getCost()) < threshold) {
                throw new InvalidRequestException(ALREADY_EXIST_ERROR);
            }
        }
        BeanUtils.copyProperties(productDto, product, "id", "categories", "manufacturer");
        Manufacturer manufacturer = getManufacturer(productDto.getManufacturer());
        product.setManufacturer(manufacturer);
        log.debug("save product to db");
        Product result = repository.saveAndFlush(product);
        if (productDto.getCategories() != null) {
            log.debug("remove old product-category associations");
            productCategoryRepository.deleteByProductId(result.getId());
            updateCategories(result, productDto.getCategories());
        }
        UpdateProductDto resultDto = new UpdateProductDto();
        BeanUtils.copyProperties(result, resultDto, "hide", "manufacturer", "categories");
        resultDto.setManufacturer(manufacturerService.convertToDto(result.getManufacturer()));
        resultDto.setCategories(categoryService.convertToDto(getCategories(result)));

        return resultDto;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public void removeProductById(Long id) {
        Product product = getProductById(id);
        productCategoryRepository.deleteByProductId(id);

        List<FileData> files = getFile(product);
        files.stream().map(FileData::getId).forEach(fileService::removeById);

        repository.delete(product);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public void hideProductById(Long id) {
        repository.hideById(id);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public void hideProductByManufacturerId(Long id) {
        repository.hideByManufacturerId(id);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public void showProductById(Long id) {
        repository.showById(id);
    }

    @Override
    public Boolean checkProduct(Long id) {
        Optional<Product> product = repository.findById(id);
        return product.filter(value -> !value.getHide()).isPresent();
    }

    @Override
    public Double getPrice(Long id) {
        Product product = getProductById(id);
        return product.getCost();
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public ProductDto convertToDto(Product product) {
        ProductDto result = new ProductDto();
        BeanUtils.copyProperties(product, result, "hide", "manufacturer");
        ManufacturerDto manufacturerDto = manufacturerService.convertToDto(product.getManufacturer());
        result.setManufacturer(manufacturerDto);
        return result;
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public List<ProductDto> convertToDto(Iterable<Product> products) {
        List<ProductDto> result = new ArrayList<>();
        for (Product product : products) {
            result.add(convertToDto(product));
        }
        return result;
    }
}
