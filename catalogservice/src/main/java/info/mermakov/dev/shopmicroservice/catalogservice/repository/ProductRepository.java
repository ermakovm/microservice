package info.mermakov.dev.shopmicroservice.catalogservice.repository;

import info.mermakov.dev.shopmicroservice.catalogservice.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    @org.springframework.data.jpa.repository.QueryHints({
            @javax.persistence.QueryHint(name = "org.hibernate.cacheable", value = "true")
    })
    @Query("from Product where name like %:name% and hide = false")
    List<Product> findByNameContains(@Param("name") String name);

    @org.springframework.data.jpa.repository.QueryHints({
            @javax.persistence.QueryHint(name = "org.hibernate.cacheable", value = "true")
    })
    @Query("from Product where hide = false")
    List<Product> findProducts();

    @org.springframework.data.jpa.repository.QueryHints({
            @javax.persistence.QueryHint(name = "org.hibernate.cacheable", value = "true")
    })
    List<Product> findByManufacturerId(Long id);

    @Modifying
    @Query("update Product set hide = true where id = :id")
    void hideById(Long id);

    @Modifying
    @Query("update Product set hide = false where id = :id")
    void showById(Long id);

    @Modifying
    @Query("update Product set hide = true where manufacturer.id = :id")
    void hideByManufacturerId(Long id);
}
