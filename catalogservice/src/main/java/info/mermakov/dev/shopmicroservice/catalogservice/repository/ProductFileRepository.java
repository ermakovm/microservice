package info.mermakov.dev.shopmicroservice.catalogservice.repository;

import info.mermakov.dev.shopmicroservice.catalogservice.model.ProductFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductFileRepository extends JpaRepository<ProductFile, Long> {
    @Modifying
    @Query("delete from ProductFile where fileData.id = :id")
    void deleteByFileId(@Param("id") Long id);

    List<ProductFile> findByProductId(Long id);
}
