package info.mermakov.dev.shopmicroservice.catalogservice.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "file")
@Data
@NoArgsConstructor
public class FileData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, length = 100)
    @Size(min = 1, max = 100)
    private String name;

    @Column(name = "path", nullable = false, length = 500)
    @Size(min = 1, max = 500)
    private String path;

    @Column(name = "description", nullable = false, length = 500)
    @Size(min = 1, max = 500)
    private String description;
}