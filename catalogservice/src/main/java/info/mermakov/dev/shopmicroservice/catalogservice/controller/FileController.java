package info.mermakov.dev.shopmicroservice.catalogservice.controller;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.FileDataDto;
import info.mermakov.dev.shopmicroservice.catalogservice.service.FileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/catalog/files")
public class FileController {
    private final FileService fileService;

    @Autowired
    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @PostMapping("/product/{id}")
    public FileDataDto uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("desc") String description, @PathVariable Long id) {
        log.debug("save file");
        return fileService.saveFile(file, description, id);
    }

    @GetMapping("/download/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        log.debug("find file for download");
        Resource resource = fileService.loadFile(fileName);
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException exception) {

        }
        if (contentType == null) {
            contentType = "application/octet-stream";
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        log.debug("remove file");
        fileService.removeById(id);
    }

    @GetMapping("/{id}")
    public FileDataDto findById(@PathVariable Long id) {
        log.debug("find File info by id");
        return fileService.findById(id);
    }

    @GetMapping
    public List<FileDataDto> findAll() {
        log.debug("Find all files info");
        return fileService.findFiles();
    }

    @PutMapping("/{id}")
    public FileDataDto updateDescription(@RequestParam("desc") String description, @PathVariable Long id) {
        log.debug("Update description by id");
        return fileService.update(description, id);
    }
}
