package info.mermakov.dev.shopmicroservice.catalogservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateProductDto {
    @Positive
    private Long id;

    @NotBlank
    @Size(min = 1, max = 100)
    private String name;

    @Positive
    @NotNull
    private Double cost;

    @Valid
    @NotNull
    private ManufacturerDto manufacturer;

    private Map<String, String> metaData;

    @Valid
    List<CategoryDto> categories;
}
