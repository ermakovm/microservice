package info.mermakov.dev.shopmicroservice.catalogservice.exception;

public class ErrorMessage {
    public static final String NO_FOUND_ERROR = "Not found any elements";
    public static final String NO_FOUND_BY_ID_ERROR = "Not found element(s) by id ";
    public static final String NO_FOUND_BY_NAME_ERROR = "Not found element(s) by name ";
    public static final String INVALID_EXTENSION_ERROR = "Invalid extension: ";
    public static final String STORAGE_ERROR = "Storage error";
    public static final String FILE_NOT_FOUND = "File not found";
    public static final String INVALID_FILE = "Invalid file";
    public static final String FIELD_INVALID_ERROR = "Invalid field ";
    public static final String ALREADY_EXIST_ERROR = "Element with specified name already exist";
    public static final String ASSOCIATED_ERROR = "You must first delete or modify products associated with element";
}
