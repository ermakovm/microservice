package info.mermakov.dev.shopmicroservice.catalogservice.controller;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.FullProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.ProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.UpdateProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/api/catalog/products")
public class ProductController {
    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public List<ProductDto> findProducts(@RequestBody(required = false) ProductDto productDto) {
        log.debug("Find products (without hide)");
        if (productDto == null) {
            return productService.findProducts();
        }
        return productService.findProductsByName(productDto.getName());
    }

    @GetMapping("/all")
    public List<ProductDto> findAllProducts() {
        log.debug("Find product (with hide)");
        return productService.findAllProducts();
    }

    @GetMapping("/{id}")
    public FullProductDto findProductById(@PathVariable Long id) {
        log.debug("Find product by id");
        return productService.findProductById(id);
    }

    @PostMapping
    public UpdateProductDto saveProduct(@Validated @RequestBody UpdateProductDto productDto) {
        log.debug("Create new product");
        productDto.setId(null);
        return productService.save(productDto);
    }

    @PutMapping("/{id}")
    public UpdateProductDto saveProduct(@PathVariable Long id, @Validated @RequestBody UpdateProductDto productDto) {
        log.debug("Update product");
        productDto.setId(id);
        return productService.save(productDto);
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable Long id) {
        log.debug("Delete product");
        productService.removeProductById(id);
    }

    @PutMapping("/{id}/hide")
    public void hideProduct(@PathVariable Long id) {
        log.debug("hide product");
        productService.hideProductById(id);
    }

    @PutMapping("/{id}/show")
    public void showProduct(@PathVariable Long id) {
        log.debug("show product");
        productService.showProductById(id);
    }

    @GetMapping("/{id}/valid")
    public Boolean checkProduct(@PathVariable Long id) {
        log.debug("Check product");
        return productService.checkProduct(id);
    }

    @GetMapping("/{id}/price")
    public Double getPrice(@PathVariable Long id) {
        log.debug("get price for product");
        return productService.getPrice(id);
    }
}
