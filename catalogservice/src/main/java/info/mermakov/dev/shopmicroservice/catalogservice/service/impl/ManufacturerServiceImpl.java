package info.mermakov.dev.shopmicroservice.catalogservice.service.impl;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.ManufacturerDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.ProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.catalogservice.model.Manufacturer;
import info.mermakov.dev.shopmicroservice.catalogservice.model.Product;
import info.mermakov.dev.shopmicroservice.catalogservice.repository.ManufacturerRepository;
import info.mermakov.dev.shopmicroservice.catalogservice.service.ManufacturerService;
import info.mermakov.dev.shopmicroservice.catalogservice.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.ALREADY_EXIST_ERROR;
import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.ASSOCIATED_ERROR;
import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.FIELD_INVALID_ERROR;
import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.NO_FOUND_BY_ID_ERROR;
import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.NO_FOUND_BY_NAME_ERROR;
import static info.mermakov.dev.shopmicroservice.catalogservice.exception.ErrorMessage.NO_FOUND_ERROR;

@Slf4j
@Service
public class ManufacturerServiceImpl implements ManufacturerService {
    private final ManufacturerRepository repository;
    private final ProductService productService;

    @Autowired
    public ManufacturerServiceImpl(ManufacturerRepository repository, @Lazy ProductService productService) {
        this.repository = repository;
        this.productService = productService;
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public List<ManufacturerDto> findAllManufacturers() {
        log.debug("Get data from db");
        List<Manufacturer> manufacturers = repository.findAll();
        if (manufacturers.size() > 0) {
            return convertToDto(manufacturers);
        }
        throw new ResourceNotFoundException(NO_FOUND_ERROR);
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public List<ManufacturerDto> findManufacturers() {
        log.debug("Get data from db");
        List<Manufacturer> manufacturers = repository.findManufacturers();
        if (manufacturers.size() > 0) {
            return convertToDto(manufacturers);
        }
        throw new ResourceNotFoundException(NO_FOUND_ERROR);
    }

    private Manufacturer getManufacturerById(Long id) {
        log.debug("get data by id from db");
        Optional<Manufacturer> manufacturer = repository.findById(id);
        return manufacturer.orElseThrow(() -> new ResourceNotFoundException(NO_FOUND_BY_ID_ERROR + id));
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public ManufacturerDto findManufacturerById(Long id) {
        return convertToDto(getManufacturerById(id));
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public Manufacturer getManufacturerOrNullByName(String name) {
        log.debug("get data from db by name");
        Optional<Manufacturer> result = repository.findByName(name);
        return result.orElse(null);
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public List<ManufacturerDto> findManufacturersByName(String name) {
        if (name == null) {
            throw new InvalidRequestException(FIELD_INVALID_ERROR + "'name'");
        }
        log.debug("find in db by name");
        List<Manufacturer> manufacturers = repository.findByNameContains(name);
        if (manufacturers.size() > 0) {
            return convertToDto(manufacturers);
        }
        throw new ResourceNotFoundException(NO_FOUND_BY_NAME_ERROR + name);
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Override
    public List<ProductDto> findProductsByManufacturerId(Long id) {
        log.debug("get data from productService");
        List<Product> result = productService.findProductsByManufacturerId(id);
        if (result.size() > 0) {
            return productService.convertToDto(result);
        }
        throw new ResourceNotFoundException(NO_FOUND_BY_ID_ERROR + id);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public Manufacturer saveManufacturer(@Validated ManufacturerDto manufacturerDto) {
        Manufacturer manufacturer;
        if (manufacturerDto.getId() != null) {
            manufacturer = getManufacturerById(manufacturerDto.getId());
        } else {
            manufacturer = new Manufacturer();
            manufacturer.setHide(false);
        }
        Manufacturer manufacturerByName = getManufacturerOrNullByName(manufacturerDto.getName());
        if (manufacturerByName != null && !manufacturerByName.getId().equals(manufacturerDto.getId())) {
            throw new InvalidRequestException(ALREADY_EXIST_ERROR);
        }
        BeanUtils.copyProperties(manufacturerDto, manufacturer, "id");
        log.debug("save manufacturer to db");
        return repository.saveAndFlush(manufacturer);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public ManufacturerDto save(@Validated ManufacturerDto manufacturerDto) {
        Manufacturer manufacturer = saveManufacturer(manufacturerDto);
        return convertToDto(manufacturer);
    }


    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public void hideManufacturerById(Long id) {
        productService.hideProductByManufacturerId(id);
        repository.hideById(id);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public void showManufacturerById(Long id) {
        repository.showById(id);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public void removeManufacturerById(Long id) {
        Manufacturer manufacturer = getManufacturerById(id);
        List<Product> products = productService.findProductsByManufacturerId(id);
        if (products.size() > 0) {
            throw new InvalidRequestException(ASSOCIATED_ERROR);
        }
        repository.delete(manufacturer);
    }

    @Override
    public ManufacturerDto convertToDto(Manufacturer manufacturer) {
        ManufacturerDto manufacturerDto = new ManufacturerDto();
        BeanUtils.copyProperties(manufacturer, manufacturerDto, "hide");
        return manufacturerDto;
    }

    @Override
    public List<ManufacturerDto> convertToDto(Iterable<Manufacturer> manufacturers) {
        List<ManufacturerDto> result = new ArrayList<>();
        for (Manufacturer manufacturer : manufacturers) {
            result.add(convertToDto(manufacturer));
        }
        return result;
    }
}
