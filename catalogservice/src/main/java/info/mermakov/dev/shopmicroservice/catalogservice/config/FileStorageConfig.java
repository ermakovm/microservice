package info.mermakov.dev.shopmicroservice.catalogservice.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class FileStorageConfig {
    @Getter
    @Value("${project.file.storage-dir}")
    private String storageDir;

    @Getter
    @Value("#{'${project.file.allowed-extension}'.split(',')}")
    private List<String> extensions;
}
