
package info.mermakov.dev.shopmicroservice.catalogservice.controller;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.ManufacturerDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.ProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.service.ManufacturerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/api/catalog/manufacturers")
public class ManufacturerController {
    private final ManufacturerService manufacturerService;

    @Autowired
    public ManufacturerController(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    @GetMapping
    public List<ManufacturerDto> findManufacturers(@RequestBody(required = false) ManufacturerDto manufacturerDto) {
        log.debug("Find manufacturers (without hide)");
        if (manufacturerDto == null) {
            return manufacturerService.findManufacturers();
        }
        return manufacturerService.findManufacturersByName(manufacturerDto.getName());
    }

    @GetMapping("/all")
    public List<ManufacturerDto> findAllManufacturers() {
        log.debug("Find manufacturers with hide");
        return manufacturerService.findAllManufacturers();
    }

    @GetMapping("/{id}")
    public ManufacturerDto findManufacturerById(@PathVariable Long id) {
        log.debug("Find manufacturer by id");
        return manufacturerService.findManufacturerById(id);
    }

    @GetMapping("/{id}/products")
    public List<ProductDto> findProductsByManufacturerId(@PathVariable Long id) {
        log.debug("Find products by manufacturer id");
        return manufacturerService.findProductsByManufacturerId(id);
    }

    @PostMapping
    public ManufacturerDto saveManufacturer(@Validated @RequestBody ManufacturerDto manufacturerDto) {
        log.debug("create new manufacturer");
        manufacturerDto.setId(null);
        return manufacturerService.save(manufacturerDto);
    }

    @PutMapping("/{id}")
    public ManufacturerDto saveManufacturer(@PathVariable Long id, @Validated @RequestBody ManufacturerDto manufacturerDto) {
        log.debug("Update manufacturer");
        manufacturerDto.setId(id);
        return manufacturerService.save(manufacturerDto);
    }

    @DeleteMapping("/{id}")
    public void deleteManufacturer(@PathVariable Long id) {
        log.debug("delete manufacturer by id");
        manufacturerService.removeManufacturerById(id);
    }

    @PutMapping("/{id}/hide")
    public void hideManufacturer(@PathVariable Long id) {
        log.debug("hide manufacturer");
        manufacturerService.hideManufacturerById(id);
    }

    @PutMapping("/{id}/show")
    public void showManufacturer(@PathVariable Long id) {
        log.debug("show manufacturer");
        manufacturerService.showManufacturerById(id);
    }
}
