package info.mermakov.dev.shopmicroservice.catalogservice.service;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.FileDataDto;
import info.mermakov.dev.shopmicroservice.catalogservice.model.FileData;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileService {
    FileDataDto saveFile(MultipartFile file, String description, Long id);

    Resource loadFile(String name);

    void removeById(Long id);

    FileDataDto findById(Long id);

    List<FileDataDto> findFiles();

    FileDataDto update(String description, Long id);

    FileDataDto convertToDto(FileData fileData);

    List<FileDataDto> convertToDto(Iterable<FileData> filesData);
}
