package info.mermakov.dev.shopmicroservice.catalogservice.repository;

import info.mermakov.dev.shopmicroservice.catalogservice.model.FileData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileDataRepository extends JpaRepository<FileData, Long> {
}
