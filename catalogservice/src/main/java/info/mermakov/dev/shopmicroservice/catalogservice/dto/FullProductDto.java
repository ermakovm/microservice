package info.mermakov.dev.shopmicroservice.catalogservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FullProductDto {
    private Long id;
    private String name;
    private Double cost;
    private ManufacturerDto manufacturer;
    private String metaData;
    private List<CategoryDto> categories;
    private List<FileDataDto> files;
}
