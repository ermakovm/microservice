package info.mermakov.dev.shopmicroservice.catalogservice.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface StorageService {
    String storeFile(MultipartFile file);

    void removeFile(String name);

    Resource loadFile(String name);
}
