package info.mermakov.dev.shopmicroservice.catalogservice.controller;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.CategoryDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.ProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.catalogservice.service.CategoryService;
import info.mermakov.dev.shopmicroservice.catalogservice.service.util.CategoryServiceTestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.CategoryServiceTestUtil.CATEGORY_DTO_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.CategoryServiceTestUtil.CATEGORY_DTO_2;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil.PRODUCT_DTO_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil.PRODUCT_DTO_2;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(CategoryController.class)
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application.properties")
public class CategoryControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private CategoryService service;

    @BeforeEach
    public void init() {
        List<CategoryDto> categoryDtos = new ArrayList<>();
        categoryDtos.add(CATEGORY_DTO_1);
        categoryDtos.add(CATEGORY_DTO_2);
        Mockito.when(service.findAllCategories()).thenReturn(categoryDtos);
        Mockito.when(service.findCategoriesByName("Category")).thenReturn(categoryDtos);
        Mockito.when(service.findCategoryById(1L)).thenReturn(CATEGORY_DTO_1);
        CategoryServiceTestUtil.initData();
    }

    @Test
    public void findCategoriesTest() throws Exception {
        mvc.perform(get("/api/catalog/categories"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].name").isNotEmpty());
    }

    @Test
    public void findCategoriesWithBodyTest() throws Exception {
        mvc.perform(get("/api/catalog/categories")
                .content("{\"name\":\"Category\"}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].name").isNotEmpty());
    }

    @Test
    public void findByIdTest() throws Exception {
        mvc.perform(get("/api/catalog/categories/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Category 1"));
    }

    @Test
    public void findProductsByCategoryIdTest() throws Exception {
        List<ProductDto> productDtos = new ArrayList<>();
        productDtos.add(PRODUCT_DTO_1);
        productDtos.add(PRODUCT_DTO_2);
        Mockito.when(service.findProductsByCategoryId(1L)).thenReturn(productDtos);

        mvc.perform(get("/api/catalog/categories/1/products"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty());
    }

    @Test
    public void saveCategoryTest() throws Exception {
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setName("Category 1");
        Mockito.when(service.save(categoryDto)).thenReturn(CATEGORY_DTO_1);

        mvc.perform(post("/api/catalog/categories")
                .content("{\"name\":\"Category 1\"}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath(".name").isNotEmpty());
    }

    @Test
    public void updateCategoryTest() throws Exception {
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setName("Category 1");
        categoryDto.setId(1L);
        Mockito.when(service.save(categoryDto)).thenReturn(CATEGORY_DTO_1);

        mvc.perform(put("/api/catalog/categories/1")
                .content("{\"name\":\"Category 1\"}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath(".name").isNotEmpty());
    }

    @Test
    public void deleteTest() throws Exception {
        mvc.perform(delete("/api/catalog/categories/1"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void ExceptionTest() throws Exception {
        Mockito.when(service.findAllCategories()).thenThrow(ResourceNotFoundException.class);
        mvc.perform(get("/api/catalog/categories"))
                .andDo(print())
                .andExpect(status().isNotFound());

        Mockito.when(service.findCategoryById(1L)).thenThrow(ResourceNotFoundException.class);
        mvc.perform(get("/api/catalog/categories/1"))
                .andDo(print())
                .andExpect(status().isNotFound());

        Mockito.when(service.save(any())).thenThrow(InvalidRequestException.class);
        mvc.perform(post("/api/catalog/categories")
                .content("{\"id\":1,\"name\":\"Category 1\"}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());

        mvc.perform(post("/api/catalog/categories")
                .content("{\"name\":\"\"}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
}
