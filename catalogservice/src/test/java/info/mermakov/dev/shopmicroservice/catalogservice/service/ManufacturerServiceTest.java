package info.mermakov.dev.shopmicroservice.catalogservice.service;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.ManufacturerDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.ProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.catalogservice.model.Manufacturer;
import info.mermakov.dev.shopmicroservice.catalogservice.model.Product;
import info.mermakov.dev.shopmicroservice.catalogservice.repository.ManufacturerRepository;
import info.mermakov.dev.shopmicroservice.catalogservice.repository.ProductRepository;
import info.mermakov.dev.shopmicroservice.catalogservice.service.util.ManufacturerServiceTestUtil;
import info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ManufacturerServiceTestUtil.MANUFACTURER_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ManufacturerServiceTestUtil.MANUFACTURER_2;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ManufacturerServiceTestUtil.MANUFACTURER_DTO_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ManufacturerServiceTestUtil.MANUFACTURER_DTO_2;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil.PRODUCT_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil.PRODUCT_2;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil.PRODUCT_DTO_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil.PRODUCT_DTO_2;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application.properties")
public class ManufacturerServiceTest {
    @Autowired
    private ManufacturerService manufacturerService;

    @MockBean
    private ManufacturerRepository repository;

    @MockBean
    private ProductRepository productRepository;

    private List<Manufacturer> manufacturers;

    @BeforeEach
    public void init() {
        ManufacturerServiceTestUtil.initData();
        ProductServiceTestUtil.initData();
        manufacturers = new ArrayList<>();
        manufacturers.add(MANUFACTURER_1);
        manufacturers.add(MANUFACTURER_2);
    }

    @Test
    public void findManufacturersTest() {
        List<Manufacturer> data = new ArrayList<>();
        data.add(MANUFACTURER_1);
        Mockito.when(repository.findManufacturers()).thenReturn(data);
        List<ManufacturerDto> result = manufacturerService.findManufacturers();
        assertEquals(1, result.size());
        assertTrue(result.contains(MANUFACTURER_DTO_1));
    }

    @Test
    public void findAllTest() {
        Mockito.when(repository.findAll()).thenReturn(manufacturers);
        List<ManufacturerDto> result = manufacturerService.findAllManufacturers();
        assertEquals(2, result.size());
        assertTrue(result.contains(MANUFACTURER_DTO_1));
        assertTrue(result.contains(MANUFACTURER_DTO_2));
    }

    @Test
    public void findManufacturersExceptionTest() {
        Mockito.when(repository.findAll()).thenReturn(new ArrayList<>());
        Mockito.when(repository.findManufacturers()).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> manufacturerService.findAllManufacturers());
        assertThrows(ResourceNotFoundException.class, () -> manufacturerService.findManufacturers());
    }

    @Test
    public void findByIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(MANUFACTURER_1));
        ManufacturerDto result = manufacturerService.findManufacturerById(1L);
        assertEquals(MANUFACTURER_DTO_1, result);
    }

    @Test
    public void findByIdExceptionTest() {
        Mockito.when(repository.findById(5L)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> manufacturerService.findManufacturerById(5L));
    }

    @Test
    public void findProductsTest() {
        List<Product> products = new ArrayList<>();
        products.add(PRODUCT_1);
        products.add(PRODUCT_2);
        Mockito.when(productRepository.findByManufacturerId(1L)).thenReturn(products);
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(MANUFACTURER_1));
        List<ProductDto> result = manufacturerService.findProductsByManufacturerId(1L);
        assertEquals(2, result.size());
        assertTrue(result.contains(PRODUCT_DTO_1));
        assertTrue(result.contains(PRODUCT_DTO_2));
    }

    @Test
    public void findProductsExceptionTest() {
        Mockito.when(productRepository.findByManufacturerId(2L)).thenReturn(new ArrayList<>());
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(MANUFACTURER_2));
        assertThrows(ResourceNotFoundException.class, () -> manufacturerService.findProductsByManufacturerId(2L));
    }

    @Test
    public void findByNameTest() {
        List<Manufacturer> data = new ArrayList<>();
        data.add(MANUFACTURER_1);
        Mockito.when(repository.findByNameContains("Manufacturer")).thenReturn(data);
        Mockito.when(repository.findByNameContains("Manufacturer4")).thenReturn(new ArrayList<>());
        List<ManufacturerDto> result = manufacturerService.findManufacturersByName("Manufacturer");
        assertEquals(1, result.size());
        assertTrue(result.contains(MANUFACTURER_DTO_1));
        assertThrows(InvalidRequestException.class, () -> manufacturerService.findManufacturersByName(null));
        assertThrows(ResourceNotFoundException.class, () -> manufacturerService.findManufacturersByName("Manufacturer4"));
    }

    @Test
    public void saveNewTest() {
        ManufacturerDto test = new ManufacturerDto();
        test.setName("Manufacturer 1");
        Mockito.when(repository.findByName("Manufacturer 1")).thenReturn(Optional.empty());
        Manufacturer data = new Manufacturer();
        data.setHide(false);
        data.setName("Manufacturer 1");
        Mockito.when(repository.saveAndFlush(data)).thenReturn(MANUFACTURER_1);

        assertEquals(MANUFACTURER_DTO_1, manufacturerService.save(test));
    }

    @Test
    public void updateTest() {
        Manufacturer expected = new Manufacturer();
        expected.setHide(false);
        expected.setName("Manufacturer 22");
        expected.setId(1L);
        Mockito.when(repository.findByName("Manufacturer 22")).thenReturn(Optional.empty());
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(MANUFACTURER_1));
        Mockito.when(repository.saveAndFlush(expected)).thenReturn(expected);
        ManufacturerDto data = new ManufacturerDto();
        data.setId(1L);
        data.setName("Manufacturer 22");
        assertEquals(data, manufacturerService.save(data));
    }

    @Test
    public void saveExceptionTest() {
        Mockito.when(repository.findByName("Manufacturer 1")).thenReturn(Optional.of(MANUFACTURER_1));
        MANUFACTURER_DTO_1.setId(null);
        assertThrows(InvalidRequestException.class, () -> manufacturerService.saveManufacturer(MANUFACTURER_DTO_1));
    }

    @Test
    public void hideAndShowTest() {
        manufacturerService.hideManufacturerById(1L);
        manufacturerService.showManufacturerById(2L);
    }

    @Test
    public void removeTest() {
        Mockito.when(productRepository.findByManufacturerId(2L)).thenReturn(new ArrayList<>());
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(MANUFACTURER_2));
        manufacturerService.removeManufacturerById(2L);
    }

    @Test
    public void removeExceptionTest() {
        List<Product> products = new ArrayList<>();
        products.add(PRODUCT_1);
        products.add(PRODUCT_2);
        Mockito.when(productRepository.findByManufacturerId(1L)).thenReturn(products);
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(MANUFACTURER_1));
        assertThrows(InvalidRequestException.class, () -> manufacturerService.removeManufacturerById(1L));
    }
}
