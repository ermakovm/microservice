package info.mermakov.dev.shopmicroservice.catalogservice.service;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.FileDataDto;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.catalogservice.model.FileData;
import info.mermakov.dev.shopmicroservice.catalogservice.repository.FileDataRepository;
import info.mermakov.dev.shopmicroservice.catalogservice.repository.ProductFileRepository;
import info.mermakov.dev.shopmicroservice.catalogservice.repository.ProductRepository;
import info.mermakov.dev.shopmicroservice.catalogservice.service.util.FileServiceTestUtil;
import info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.FileServiceTestUtil.FILE_DATA;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.FileServiceTestUtil.FILE_DATA_DTO;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil.PRODUCT_1;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application.properties")
public class FileServiceTest {
    @Autowired
    private FileService fileService;

    @MockBean
    private StorageService storageService;

    @MockBean
    private FileDataRepository repository;

    @MockBean
    private ProductFileRepository productFileRepository;

    @MockBean
    private ProductRepository productRepository;

    private List<FileData> files;

    @BeforeEach
    public void init() {
        files = new ArrayList<>();
        files.add(FILE_DATA);
        FileServiceTestUtil.initData();
        ProductServiceTestUtil.initData();
    }

    @Test
    public void findFilesTest() {
        Mockito.when(repository.findAll()).thenReturn(files);
        List<FileDataDto> result = fileService.findFiles();
        assertEquals(1, result.size());
        assertTrue(result.contains(FILE_DATA_DTO));
    }

    @Test
    public void findFilesExceptionTest() {
        Mockito.when(repository.findAll()).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> fileService.findFiles());
    }

    @Test
    public void findByIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(FILE_DATA));
        Mockito.when(repository.findById(2L)).thenReturn(Optional.empty());
        assertEquals(FILE_DATA_DTO, fileService.findById(1L));
        assertThrows(ResourceNotFoundException.class, () -> fileService.findById(2L));
    }

    @Test
    public void removeTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(FILE_DATA));
        fileService.removeById(1L);
    }

    @Test
    public void updateTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(FILE_DATA));
        FILE_DATA_DTO.setDescription("New description");
        FileData expected = new FileData();
        expected.setId(1L);
        expected.setDescription("New description");
        expected.setName("name.pdf");
        expected.setPath("/api/catalog/files/download/name.pdf");
        Mockito.when(repository.saveAndFlush(expected)).thenReturn(expected);
        assertEquals(FILE_DATA_DTO, fileService.update("New description", 1L));
    }

    @Test
    public void saveTest() {
        MockMultipartFile file = new MockMultipartFile("test-file", "name.pdf", null, "Test data".getBytes());
        Mockito.when(storageService.storeFile(file)).thenReturn("name.pdf");
        FileData data = new FileData();
        data.setDescription("Description");
        data.setName("name.pdf");
        data.setPath("/api/catalog/files/download/name.pdf");
        Mockito.when(repository.saveAndFlush(data)).thenReturn(FILE_DATA);
        Mockito.when(productRepository.findById(1L)).thenReturn(Optional.of(PRODUCT_1));
        assertEquals(FILE_DATA_DTO, fileService.saveFile(file, "Description", 1L));
    }
}
