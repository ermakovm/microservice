package info.mermakov.dev.shopmicroservice.catalogservice.service.util;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.FileDataDto;
import info.mermakov.dev.shopmicroservice.catalogservice.model.FileData;

public class FileServiceTestUtil {
    public static final FileData FILE_DATA;
    public static final FileDataDto FILE_DATA_DTO;

    static {
        FILE_DATA = new FileData();
        FILE_DATA_DTO = new FileDataDto();
        initData();
    }

    public static void initData() {
        FILE_DATA.setId(1L);
        FILE_DATA.setDescription("Description");
        FILE_DATA.setName("name.pdf");
        FILE_DATA.setPath("/api/catalog/files/download/name.pdf");

        FILE_DATA_DTO.setId(1L);
        FILE_DATA_DTO.setDescription("Description");
        FILE_DATA_DTO.setName("name.pdf");
        FILE_DATA_DTO.setPath("/api/catalog/files/download/name.pdf");
    }
}
