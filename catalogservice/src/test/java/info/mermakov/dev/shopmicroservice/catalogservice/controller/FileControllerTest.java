package info.mermakov.dev.shopmicroservice.catalogservice.controller;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.FileDataDto;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.catalogservice.service.FileService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.UrlResource;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.FileServiceTestUtil.FILE_DATA_DTO;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(FileController.class)
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application.properties")
public class FileControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private FileService service;

    @BeforeEach
    public void init() {
        List<FileDataDto> result = new ArrayList<>();
        result.add(FILE_DATA_DTO);
        Mockito.when(service.findFiles()).thenReturn(result);
    }

    @Test
    public void deleteTest() throws Exception {
        mvc.perform(delete("/api/catalog/files/1"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getByIdTest() throws Exception {
        Mockito.when(service.findById(any())).thenReturn(FILE_DATA_DTO);
        mvc.perform(get("/api/catalog/files/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").isNotEmpty());
    }

    @Test
    public void findAllTest() throws Exception {
        mvc.perform(get("/api/catalog/files"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].name").isNotEmpty());
    }

    @Test
    public void downloadTest() throws Exception {
        Mockito.when(service.loadFile(any())).thenReturn(new UrlResource("http://yandex.ru"));
        mvc.perform(get("/api/catalog/files/download/test.pdf"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void updateTest() throws Exception {
        mvc.perform(put("/api/catalog/files/1").param("desc", "Description"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void exceptionTest() throws Exception {
        Mockito.when(service.findById(any())).thenThrow(ResourceNotFoundException.class);
        mvc.perform(get("/api/catalog/files/1"))
                .andDo(print())
                .andExpect(status().isNotFound());

        Mockito.when(service.update(any(), any())).thenThrow(InvalidRequestException.class);
        mvc.perform(put("/api/catalog/files/1")
                .param("desc", ""))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
}
