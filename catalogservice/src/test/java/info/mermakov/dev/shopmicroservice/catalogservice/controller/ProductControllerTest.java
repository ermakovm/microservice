package info.mermakov.dev.shopmicroservice.catalogservice.controller;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.CategoryDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.FileDataDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.FullProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.ProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.UpdateProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.catalogservice.service.ProductService;
import info.mermakov.dev.shopmicroservice.catalogservice.service.util.CategoryServiceTestUtil;
import info.mermakov.dev.shopmicroservice.catalogservice.service.util.FileServiceTestUtil;
import info.mermakov.dev.shopmicroservice.catalogservice.service.util.ManufacturerServiceTestUtil;
import info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.CategoryServiceTestUtil.CATEGORY_DTO_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.FileServiceTestUtil.FILE_DATA_DTO;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ManufacturerServiceTestUtil.MANUFACTURER_DTO_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil.PRODUCT_DTO_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil.PRODUCT_DTO_2;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductController.class)
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application.properties")
public class ProductControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductService service;

    @BeforeEach
    public void init() {
        List<ProductDto> result = new ArrayList<>();
        result.add(PRODUCT_DTO_1);
        result.add(PRODUCT_DTO_2);

        Mockito.when(service.findAllProducts()).thenReturn(result);
        Mockito.when(service.findProducts()).thenReturn(result);
        Mockito.when(service.findProductsByName(any())).thenReturn(result);

        ManufacturerServiceTestUtil.initData();
        ProductServiceTestUtil.initData();
        CategoryServiceTestUtil.initData();
        FileServiceTestUtil.initData();
    }

    @Test
    public void findProductTest() throws Exception {
        mvc.perform(get("/api/catalog/products"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath(".[*].name").isNotEmpty());
    }

    @Test
    public void findProductsWithBodyTest() throws Exception {
        mvc.perform(get("/api/catalog/products")
                .content("{\"name\":\"Produc\"}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath(".[*].name").isNotEmpty());
    }

    @Test
    public void findByIdTest() throws Exception {
        FullProductDto expected = new FullProductDto();
        List<CategoryDto> categoriesDto = new ArrayList<>();
        categoriesDto.add(CATEGORY_DTO_1);
        expected.setName("Product 1");
        expected.setId(1L);
        expected.setManufacturer(MANUFACTURER_DTO_1);
        expected.setCost(123d);
        expected.setCategories(categoriesDto);
        List<FileDataDto> files = new ArrayList<>();
        files.add(FILE_DATA_DTO);
        expected.setFiles(files);

        Mockito.when(service.findProductById(1L)).thenReturn(expected);
        mvc.perform(get("/api/catalog/products/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Product 1"));
    }

    @Test
    public void findAllTest() throws Exception {
        mvc.perform(get("/api/catalog/products/all"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath(".[*].name").isNotEmpty());
    }

    @Test
    public void saveProductTest() throws Exception {
        UpdateProductDto productDto = new UpdateProductDto();
        productDto.setId(1L);
        productDto.setName("Product 22");
        productDto.setCost(123d);
        productDto.setManufacturer(MANUFACTURER_DTO_1);
        List<CategoryDto> categoryDtos = new ArrayList<>();
        categoryDtos.add(CATEGORY_DTO_1);
        productDto.setCategories(categoryDtos);
        Mockito.when(service.save(any())).thenReturn(productDto);

        mvc.perform(post("/api/catalog/products")
                .content("{\"id\":1,\"name\":\"Product 22\",\"cost\":123.0,\"manufacturer\":{\"id\":1,\"name\":\"Manufacturer 1\"},\"metaData\":null,\"categories\":[{\"id\":1,\"name\":\"Category 1\"}]}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").isNotEmpty());
    }

    @Test
    public void updateManufacturerTest() throws Exception {
        UpdateProductDto productDto = new UpdateProductDto();
        productDto.setId(1L);
        productDto.setName("Product 22");
        productDto.setCost(123d);
        productDto.setManufacturer(MANUFACTURER_DTO_1);
        List<CategoryDto> categoryDtos = new ArrayList<>();
        categoryDtos.add(CATEGORY_DTO_1);
        productDto.setCategories(categoryDtos);
        Mockito.when(service.save(any())).thenReturn(productDto);

        mvc.perform(put("/api/catalog/products/1")
                .content("{\"id\":1,\"name\":\"Product 22\",\"cost\":123.0,\"manufacturer\":{\"id\":1,\"name\":\"Manufacturer 1\"},\"metaData\":null,\"categories\":[{\"id\":1,\"name\":\"Category 1\"}]}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").isNotEmpty());
    }

    @Test
    public void deleteTest() throws Exception {
        mvc.perform(delete("/api/catalog/products/1"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void ExceptionTest() throws Exception {
        Mockito.when(service.findAllProducts()).thenThrow(ResourceNotFoundException.class);
        mvc.perform(get("/api/catalog/products/all"))
                .andDo(print())
                .andExpect(status().isNotFound());

        Mockito.when(service.findProductById(1L)).thenThrow(ResourceNotFoundException.class);
        mvc.perform(get("/api/catalog/products/1"))
                .andDo(print())
                .andExpect(status().isNotFound());

        Mockito.when(service.save(any())).thenThrow(InvalidRequestException.class);
        mvc.perform(post("/api/catalog/products")
                .content("{\"id\":1,\"name\":\"Manufacturer 1\"}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());

        mvc.perform(post("/api/catalog/products")
                .content("{\"id\":1,\"name\":\"\",\"cost\":123.0,\"manufacturer\":{\"id\":1,\"name\":\"Manufacturer 1\"},\"metaData\":null,\"categories\":[{\"id\":1,\"name\":\"Category 1\"}]}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void hideAndShowTest() throws Exception {
        mvc.perform(put("/api/catalog/products/1/hide"))
                .andDo(print())
                .andExpect(status().isOk());

        mvc.perform(put("/api/catalog/products/1/show"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void checkAndGetTest() throws Exception {
        Mockito.when(service.checkProduct(any())).thenReturn(true);
        Mockito.when(service.getPrice(any())).thenReturn(123d);
        mvc.perform(get("/api/catalog/products/1/valid"))
                .andDo(print())
                .andExpect(status().isOk());

        mvc.perform(get("/api/catalog/products/1/price"))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
