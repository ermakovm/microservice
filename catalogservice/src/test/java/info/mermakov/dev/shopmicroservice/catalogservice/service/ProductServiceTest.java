package info.mermakov.dev.shopmicroservice.catalogservice.service;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.CategoryDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.FileDataDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.FullProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.ProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.UpdateProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.catalogservice.model.Product;
import info.mermakov.dev.shopmicroservice.catalogservice.model.ProductCategory;
import info.mermakov.dev.shopmicroservice.catalogservice.model.ProductFile;
import info.mermakov.dev.shopmicroservice.catalogservice.repository.CategoryRepository;
import info.mermakov.dev.shopmicroservice.catalogservice.repository.ManufacturerRepository;
import info.mermakov.dev.shopmicroservice.catalogservice.repository.ProductCategoryRepository;
import info.mermakov.dev.shopmicroservice.catalogservice.repository.ProductFileRepository;
import info.mermakov.dev.shopmicroservice.catalogservice.repository.ProductRepository;
import info.mermakov.dev.shopmicroservice.catalogservice.service.util.CategoryServiceTestUtil;
import info.mermakov.dev.shopmicroservice.catalogservice.service.util.FileServiceTestUtil;
import info.mermakov.dev.shopmicroservice.catalogservice.service.util.ManufacturerServiceTestUtil;
import info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.CategoryServiceTestUtil.CATEGORY_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.CategoryServiceTestUtil.CATEGORY_DTO_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.FileServiceTestUtil.FILE_DATA;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.FileServiceTestUtil.FILE_DATA_DTO;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ManufacturerServiceTestUtil.MANUFACTURER_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ManufacturerServiceTestUtil.MANUFACTURER_DTO_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil.PRODUCT_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil.PRODUCT_2;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil.PRODUCT_DTO_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil.PRODUCT_DTO_2;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application.properties")
public class ProductServiceTest {
    @Autowired
    private ProductService productService;

    @MockBean
    private ProductRepository repository;

    @MockBean
    private ProductCategoryRepository productCategoryRepository;

    @MockBean
    private ProductFileRepository productFileRepository;

    @MockBean
    private CategoryRepository categoryRepository;

    @MockBean
    private ManufacturerRepository manufacturerRepository;

    private List<Product> products;

    @BeforeEach
    public void init() {
        products = new ArrayList<>();
        products.add(PRODUCT_1);
        products.add(PRODUCT_2);
        ProductServiceTestUtil.initData();
        CategoryServiceTestUtil.initData();
        ManufacturerServiceTestUtil.initData();
        FileServiceTestUtil.initData();
    }

    @Test
    public void findProductsTest() {
        List<Product> data = new ArrayList<>();
        data.add(PRODUCT_1);
        Mockito.when(repository.findProducts()).thenReturn(data);
        List<ProductDto> result = productService.findProducts();
        assertEquals(1, result.size());
        assertTrue(result.contains(PRODUCT_DTO_1));
    }

    @Test
    public void findAllTest() {
        Mockito.when(repository.findAll()).thenReturn(products);
        List<ProductDto> result = productService.findAllProducts();
        assertEquals(2, result.size());
        assertTrue(result.contains(PRODUCT_DTO_1));
        assertTrue(result.contains(PRODUCT_DTO_2));
    }

    @Test
    public void findProductExceptionTest() {
        Mockito.when(repository.findAll()).thenReturn(new ArrayList<>());
        Mockito.when(repository.findProducts()).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> productService.findProducts());
        assertThrows(ResourceNotFoundException.class, () -> productService.findAllProducts());
    }

    @Test
    public void findByIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(PRODUCT_1));

        ProductCategory data = new ProductCategory();
        data.setId(1L);
        data.setCategory(CATEGORY_1);
        data.setProduct(PRODUCT_1);
        List<ProductCategory> testData = new ArrayList<>();
        testData.add(data);
        Mockito.when(productCategoryRepository.findByProductId(1L)).thenReturn(testData);

        ProductFile productFile = new ProductFile();
        productFile.setId(1L);
        productFile.setProduct(PRODUCT_1);
        productFile.setFileData(FILE_DATA);
        List<ProductFile> productFiles = new ArrayList<>();
        productFiles.add(productFile);
        Mockito.when(productFileRepository.findByProductId(1L)).thenReturn(productFiles);

        FullProductDto expected = new FullProductDto();
        List<CategoryDto> categoriesDto = new ArrayList<>();
        categoriesDto.add(CATEGORY_DTO_1);
        expected.setName("Product 1");
        expected.setId(1L);
        expected.setManufacturer(MANUFACTURER_DTO_1);
        expected.setCost(123d);
        expected.setCategories(categoriesDto);
        List<FileDataDto> files = new ArrayList<>();
        files.add(FILE_DATA_DTO);
        expected.setFiles(files);

        assertEquals(expected, productService.findProductById(1L));
    }

    @Test
    public void findByIdExceptionTest() {
        Mockito.when(repository.findById(3L)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> productService.findProductById(3L));
    }

    @Test
    public void findByNameTest() {
        List<Product> data = new ArrayList<>();
        data.add(PRODUCT_1);
        Mockito.when(repository.findByNameContains("Product")).thenReturn(data);
        List<ProductDto> result = productService.findProductsByName("Product");
        assertEquals(1, result.size());
        assertTrue(result.contains(PRODUCT_DTO_1));
    }

    @Test
    public void findByNameExceptionTest() {
        Mockito.when(repository.findByNameContains("Product 3")).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> productService.findProductsByName("Product 3"));
        assertThrows(InvalidRequestException.class, () -> productService.findProductsByName(null));
    }

    @Test
    public void saveTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(PRODUCT_1));
        Mockito.when(categoryRepository.findByName("Category 1")).thenReturn(Optional.of(CATEGORY_1));
        Mockito.when(manufacturerRepository.findByName("Manufacturer 1")).thenReturn(Optional.of(MANUFACTURER_1));

        UpdateProductDto productDto = new UpdateProductDto();
        productDto.setId(1L);
        productDto.setName("Product 22");
        productDto.setCost(123d);
        productDto.setManufacturer(MANUFACTURER_DTO_1);
        List<CategoryDto> categoryDtos = new ArrayList<>();
        categoryDtos.add(CATEGORY_DTO_1);
        productDto.setCategories(categoryDtos);

        ProductCategory data = new ProductCategory();
        data.setId(1L);
        data.setCategory(CATEGORY_1);
        data.setProduct(PRODUCT_1);
        List<ProductCategory> testData = new ArrayList<>();
        testData.add(data);
        Mockito.when(productCategoryRepository.findByProductId(1L)).thenReturn(testData);

        Mockito.when(repository.saveAndFlush(PRODUCT_1)).thenReturn(PRODUCT_1);

        assertEquals(productDto, productService.save(productDto));
    }

    @Test
    public void saveExceptionTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(PRODUCT_1));
        List<Product> data = new ArrayList<>();
        data.add(PRODUCT_1);
        Mockito.when(repository.findByNameContains("Product 1")).thenReturn(data);
        UpdateProductDto productDto = new UpdateProductDto();
        productDto.setId(null);
        productDto.setName("Product 1");
        productDto.setCost(123d);
        productDto.setManufacturer(MANUFACTURER_DTO_1);
        List<CategoryDto> categoryDtos = new ArrayList<>();
        categoryDtos.add(CATEGORY_DTO_1);
        productDto.setCategories(categoryDtos);

        assertThrows(InvalidRequestException.class, () -> productService.save(productDto));
    }

    @Test
    public void showAndHideTest() {
        productService.showProductById(1L);
        productService.hideProductById(1L);
        productService.hideProductByManufacturerId(1L);
    }

    @Test
    public void removeTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(PRODUCT_1));
        productService.removeProductById(1L);
    }

    @Test
    public void checkProductTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(PRODUCT_1));
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(PRODUCT_2));
        Mockito.when(repository.findById(3L)).thenReturn(Optional.empty());
        assertFalse(productService.checkProduct(2L));
        assertTrue(productService.checkProduct(1L));
        assertFalse(productService.checkProduct(3L));
    }

    @Test
    public void getCostTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(PRODUCT_1));
        assertEquals(123d, productService.getPrice(1L));
    }
}
