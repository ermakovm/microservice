package info.mermakov.dev.shopmicroservice.catalogservice.service.util;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.CategoryDto;
import info.mermakov.dev.shopmicroservice.catalogservice.model.Category;

public class CategoryServiceTestUtil {
    public static final Category CATEGORY_1;
    public static final Category CATEGORY_2;
    public static final CategoryDto CATEGORY_DTO_1;
    public static final CategoryDto CATEGORY_DTO_2;

    static {
        CATEGORY_1 = new Category();
        CATEGORY_2 = new Category();
        CATEGORY_DTO_1 = new CategoryDto();
        CATEGORY_DTO_2 = new CategoryDto();
        initData();
    }

    public static void initData() {
        CATEGORY_1.setId(1L);
        CATEGORY_1.setName("Category 1");
        CATEGORY_2.setId(2L);
        CATEGORY_2.setName("Category 2");

        CATEGORY_DTO_1.setId(1L);
        CATEGORY_DTO_1.setName("Category 1");
        CATEGORY_DTO_2.setId(2L);
        CATEGORY_DTO_2.setName("Category 2");
    }
}
