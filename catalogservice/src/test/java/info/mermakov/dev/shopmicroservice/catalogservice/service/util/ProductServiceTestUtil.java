package info.mermakov.dev.shopmicroservice.catalogservice.service.util;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.ProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.model.Product;

import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ManufacturerServiceTestUtil.MANUFACTURER_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ManufacturerServiceTestUtil.MANUFACTURER_DTO_1;

public class ProductServiceTestUtil {
    public static final Product PRODUCT_1;
    public static final Product PRODUCT_2;
    public static final ProductDto PRODUCT_DTO_1;
    public static final ProductDto PRODUCT_DTO_2;

    static {
        PRODUCT_1 = new Product();
        PRODUCT_2 = new Product();
        PRODUCT_DTO_1 = new ProductDto();
        PRODUCT_DTO_2 = new ProductDto();

        initData();
    }

    public static void initData() {
        PRODUCT_1.setName("Product 1");
        PRODUCT_1.setHide(false);
        PRODUCT_1.setId(1L);
        PRODUCT_1.setManufacturer(MANUFACTURER_1);
        PRODUCT_1.setCost(123d);

        PRODUCT_2.setName("Product 2");
        PRODUCT_2.setHide(true);
        PRODUCT_2.setId(2L);
        PRODUCT_2.setManufacturer(MANUFACTURER_1);
        PRODUCT_2.setCost(1234d);

        PRODUCT_DTO_1.setName("Product 1");
        PRODUCT_DTO_1.setId(1L);
        PRODUCT_DTO_1.setManufacturer(MANUFACTURER_DTO_1);
        PRODUCT_DTO_1.setCost(123d);

        PRODUCT_DTO_2.setName("Product 2");
        PRODUCT_DTO_2.setId(2L);
        PRODUCT_DTO_2.setManufacturer(MANUFACTURER_DTO_1);
        PRODUCT_DTO_2.setCost(1234d);
    }
}
