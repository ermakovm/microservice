package info.mermakov.dev.shopmicroservice.catalogservice.controller;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.ManufacturerDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.ProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.catalogservice.service.ManufacturerService;
import info.mermakov.dev.shopmicroservice.catalogservice.service.util.ManufacturerServiceTestUtil;
import info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ManufacturerServiceTestUtil.MANUFACTURER_DTO_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ManufacturerServiceTestUtil.MANUFACTURER_DTO_2;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil.PRODUCT_DTO_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil.PRODUCT_DTO_2;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ManufacturerController.class)
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application.properties")
public class ManufacturerControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private ManufacturerService service;

    @BeforeEach
    public void init() {
        List<ManufacturerDto> result = new ArrayList<>();
        result.add(MANUFACTURER_DTO_1);
        result.add(MANUFACTURER_DTO_2);
        Mockito.when(service.findAllManufacturers()).thenReturn(result);
        Mockito.when(service.findManufacturers()).thenReturn(result);
        Mockito.when(service.findManufacturersByName(any())).thenReturn(result);
        ManufacturerServiceTestUtil.initData();
        ProductServiceTestUtil.initData();
    }

    @Test
    public void findManufacturersTest() throws Exception {
        mvc.perform(get("/api/catalog/manufacturers"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath(".[*].name").isNotEmpty());
    }

    @Test
    public void findManufacturersWithBodyTest() throws Exception {
        mvc.perform(get("/api/catalog/manufacturers")
                .content("{\"name\":\"Manuf\"}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath(".[*].name").isNotEmpty());
    }

    @Test
    public void findByIdTest() throws Exception {
        Mockito.when(service.findManufacturerById(1L)).thenReturn(MANUFACTURER_DTO_1);
        mvc.perform(get("/api/catalog/manufacturers/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Manufacturer 1"));
    }

    @Test
    public void findAllTest() throws Exception {
        mvc.perform(get("/api/catalog/manufacturers/all"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath(".[*].name").isNotEmpty());
    }

    @Test
    public void findProductsByCategoryIdTest() throws Exception {
        List<ProductDto> productDtos = new ArrayList<>();
        productDtos.add(PRODUCT_DTO_1);
        productDtos.add(PRODUCT_DTO_2);
        Mockito.when(service.findProductsByManufacturerId(1L)).thenReturn(productDtos);

        mvc.perform(get("/api/catalog/manufacturers/1/products"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").isNotEmpty());
    }


    @Test
    public void saveManufacturerTest() throws Exception {
        ManufacturerDto manufacturerDto = new ManufacturerDto();
        manufacturerDto.setName("Manufacturer 1");
        Mockito.when(service.save(manufacturerDto)).thenReturn(MANUFACTURER_DTO_1);

        mvc.perform(post("/api/catalog/manufacturers")
                .content("{\"name\":\"Manufacturer 1\"}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath(".name").isNotEmpty());
    }

    @Test
    public void updateManufacturerTest() throws Exception {
        ManufacturerDto manufacturerDto = new ManufacturerDto();
        manufacturerDto.setName("Manufacturer 1");
        manufacturerDto.setId(1L);
        Mockito.when(service.save(manufacturerDto)).thenReturn(MANUFACTURER_DTO_1);

        mvc.perform(put("/api/catalog/manufacturers/1")
                .content("{\"name\":\"Manufacturer 1\"}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath(".name").isNotEmpty());
    }

    @Test
    public void deleteTest() throws Exception {
        mvc.perform(delete("/api/catalog/manufacturers/1"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void ExceptionTest() throws Exception {
        Mockito.when(service.findAllManufacturers()).thenThrow(ResourceNotFoundException.class);
        mvc.perform(get("/api/catalog/manufacturers/all"))
                .andDo(print())
                .andExpect(status().isNotFound());

        Mockito.when(service.findManufacturerById(1L)).thenThrow(ResourceNotFoundException.class);
        mvc.perform(get("/api/catalog/manufacturers/1"))
                .andDo(print())
                .andExpect(status().isNotFound());

        Mockito.when(service.save(any())).thenThrow(InvalidRequestException.class);
        mvc.perform(post("/api/catalog/manufacturers")
                .content("{\"id\":1,\"name\":\"Manufacturer 1\"}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());

        mvc.perform(put("/api/catalog/manufacturers/1")
                .content("{\"name\":\"\"}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void hideAndShowTest() throws Exception {
        mvc.perform(put("/api/catalog/manufacturers/1/hide"))
                .andDo(print())
                .andExpect(status().isOk());

        mvc.perform(put("/api/catalog/manufacturers/1/show"))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
