package info.mermakov.dev.shopmicroservice.catalogservice.service.util;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.ManufacturerDto;
import info.mermakov.dev.shopmicroservice.catalogservice.model.Manufacturer;

public class ManufacturerServiceTestUtil {
    public static final Manufacturer MANUFACTURER_1;
    public static final Manufacturer MANUFACTURER_2;
    public static final ManufacturerDto MANUFACTURER_DTO_1;
    public static final ManufacturerDto MANUFACTURER_DTO_2;

    static {
        MANUFACTURER_1 = new Manufacturer();
        MANUFACTURER_2 = new Manufacturer();

        MANUFACTURER_DTO_1 = new ManufacturerDto();
        MANUFACTURER_DTO_2 = new ManufacturerDto();
    }

    public static void initData() {
        MANUFACTURER_1.setId(1L);
        MANUFACTURER_1.setName("Manufacturer 1");
        MANUFACTURER_1.setHide(false);

        MANUFACTURER_2.setId(2L);
        MANUFACTURER_2.setName("Manufacturer 2");
        MANUFACTURER_2.setHide(true);

        MANUFACTURER_DTO_1.setId(1L);
        MANUFACTURER_DTO_1.setName("Manufacturer 1");

        MANUFACTURER_DTO_2.setId(2L);
        MANUFACTURER_DTO_2.setName("Manufacturer 2");
    }
}
