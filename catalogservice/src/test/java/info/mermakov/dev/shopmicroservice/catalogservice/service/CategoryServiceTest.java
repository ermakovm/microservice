package info.mermakov.dev.shopmicroservice.catalogservice.service;

import info.mermakov.dev.shopmicroservice.catalogservice.dto.CategoryDto;
import info.mermakov.dev.shopmicroservice.catalogservice.dto.ProductDto;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.InvalidRequestException;
import info.mermakov.dev.shopmicroservice.catalogservice.exception.ResourceNotFoundException;
import info.mermakov.dev.shopmicroservice.catalogservice.model.Category;
import info.mermakov.dev.shopmicroservice.catalogservice.model.ProductCategory;
import info.mermakov.dev.shopmicroservice.catalogservice.repository.CategoryRepository;
import info.mermakov.dev.shopmicroservice.catalogservice.repository.ProductCategoryRepository;
import info.mermakov.dev.shopmicroservice.catalogservice.service.util.CategoryServiceTestUtil;
import info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.CategoryServiceTestUtil.CATEGORY_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.CategoryServiceTestUtil.CATEGORY_2;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.CategoryServiceTestUtil.CATEGORY_DTO_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.CategoryServiceTestUtil.CATEGORY_DTO_2;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil.PRODUCT_1;
import static info.mermakov.dev.shopmicroservice.catalogservice.service.util.ProductServiceTestUtil.PRODUCT_DTO_1;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application.properties")
public class CategoryServiceTest {
    @MockBean
    private CategoryRepository repository;

    @MockBean
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private CategoryService categoryService;

    private List<Category> categories;

    @BeforeEach
    public void init() {
        CategoryServiceTestUtil.initData();
        ProductServiceTestUtil.initData();
        categories = new ArrayList<>();
        categories.add(CATEGORY_1);
        categories.add(CATEGORY_2);
    }

    @Test
    public void findAllCategoriesTest() {
        Mockito.when(repository.findAll()).thenReturn(categories);
        List<CategoryDto> result = categoryService.findAllCategories();
        assertEquals(2, result.size());
        assertTrue(result.contains(CATEGORY_DTO_1));
        assertTrue(result.contains(CATEGORY_DTO_2));
    }

    @Test
    public void findAllCategoriesExceptionTest() {
        Mockito.when(repository.findAll()).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> categoryService.findAllCategories());
    }

    @Test
    public void findCategoryByIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(CATEGORY_1));
        assertEquals(CATEGORY_DTO_1, categoryService.findCategoryById(1L));
    }

    @Test
    public void findCategoryByIdExceptionTest() {
        Mockito.when(repository.findById(2L)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> categoryService.findCategoryById(2L));
    }

    @Test
    public void getCategoryOrNullByNameTest() {
        Mockito.when(repository.findByName("Category 1")).thenReturn(Optional.of(CATEGORY_1));
        Mockito.when(repository.findByName("Category 2")).thenReturn(Optional.empty());
        assertEquals(CATEGORY_1, categoryService.getCategoryOrNullByName("Category 1"));
        assertNull(categoryService.getCategoryOrNullByName("Category 2"));
    }

    @Test
    public void findCategoriesByNameTest() {
        Mockito.when(repository.findByNameContains("Category")).thenReturn(categories);
        List<CategoryDto> result = categoryService.findCategoriesByName("Category");
        assertEquals(2, result.size());
        assertTrue(result.contains(CATEGORY_DTO_1));
        assertTrue(result.contains(CATEGORY_DTO_2));
    }

    @Test
    public void findCategoriesByNameExceptionTest() {
        assertThrows(InvalidRequestException.class, () -> categoryService.findCategoriesByName(null));
        Mockito.when(repository.findByNameContains("Category 2")).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> categoryService.findCategoriesByName("Category 2"));
    }

    @Test
    public void findProductByCategoryIdTest() {
        ProductCategory data = new ProductCategory();
        data.setId(1L);
        data.setCategory(CATEGORY_1);
        data.setProduct(PRODUCT_1);
        List<ProductCategory> testData = new ArrayList<>();
        testData.add(data);
        Mockito.when(productCategoryRepository.findByCategoryId(1L)).thenReturn(testData);
        List<ProductDto> result = categoryService.findProductsByCategoryId(1L);
        assertEquals(1, result.size());
        assertTrue(result.contains(PRODUCT_DTO_1));
    }

    @Test
    public void findProductByCategoryIdExceptionTest() {
        Mockito.when(productCategoryRepository.findByCategoryId(2L)).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> categoryService.findProductsByCategoryId(2L));
    }

    @Test
    public void saveTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(CATEGORY_1));
        Mockito.when(repository.findByName("Category 2")).thenReturn(Optional.empty());
        Mockito.when(repository.saveAndFlush(CATEGORY_1)).thenReturn(CATEGORY_1);
        CategoryDto categoryDto = new CategoryDto(1L, "Category 3");
        CategoryDto result = categoryService.save(categoryDto);
        assertEquals(categoryDto, result);
    }

    @Test
    public void saveNewTest() {
        Mockito.when(repository.findByName("Category 1")).thenReturn(Optional.empty());
        Category category = new Category();
        BeanUtils.copyProperties(CATEGORY_1, category, "id");
        Mockito.when(repository.saveAndFlush(category)).thenReturn(CATEGORY_1);
        CategoryDto categoryDto = new CategoryDto(null, "Category 1");
        CategoryDto result = categoryService.save(categoryDto);
        assertEquals(CATEGORY_DTO_1, result);
    }

    @Test
    public void saveExceptionTest() {
        Mockito.when(repository.findByName("Category 2")).thenReturn(Optional.of(CATEGORY_2));
        CategoryDto categoryDto = new CategoryDto(null, "Category 2");
        assertThrows(InvalidRequestException.class, () -> categoryService.save(categoryDto));
    }

    @Test
    public void removeCategoryByIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(CATEGORY_1));
        categoryService.removeCategoryById(1L);
    }
}
